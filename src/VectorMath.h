/*
 * Vector and Matrix operations (along with various other constants and maths
 * stuff...)
 */

#ifndef RC_VECTORMATH_H
#define RC_VECTORMATH_H


#define smax2(a, b)			(((a) > (b)) ? (a) : (b))
#define smin2(a, b)			(((a) < (b)) ? (a) : (b))

#define smax3(a, b, c)		(((a) > (b)) ? ((a) > (c) ? (a) : (c)) : ((b) > (c) ? (b) : (c)))
#define smin3(a, b, c)		(((a) < (b)) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)))


/*===========================================================[ Constants ]====*/

#define fPI					3.141592653589793238462643383280f
#define f2PI				6.283185307179586476925286766560f
#define fPIinv				0.318309886183790671537767526745f
#define f2PIinv				0.159154943091895335768883763373f

#define fMin				1.17549e-038f
#define fMax				3.40282e+038f

/*==============================================================[ Scalar ]====*/

#define sclamp(s, a, b)			((s) > (b) ? (b) : ((s) < (a) ? (a) : (s)))

#define ssaturate(s)			sclamp((s), 0.0f, 1.0f)

#define sswap(a, b)				T tmp##a##b = (a); (a) = (b); (b) = tmp##a##b

#define toRadf(a)				((a) * 0.01745329251994329576923690768489f)
#define toDegf(a)				((a) * 57.29577951308232087679815481428f)

#define toRadd(a)				((a) * 0.01745329251994329576923690768489)
#define toDegd(a)				((a) * 57.29577951308232087679815481428)

/*===========================================================[ 2D Vector ]====*/

template<class T> class vec2;
template<class T> class vec3;
template<class T> class vec4;

template<class T> class mat2;
template<class T> class mat3;
template<class T> class mat4;


template<class T> 
class vec2
{
	public:

		/* Members */
		union {
			struct {
				union {
					T		x;
					T		u;
				};
				union {
					T		y;
					T		v;
				};
			};

			T				vec[2];
		};

		/* Constructors */
		vec2() {}

		vec2(
			const vec2<T>	&v
			) : x(v.x), y(v.y) {}

		vec2(
				T			nx,
				T			ny
			) : x(nx), y(ny) {}

		/* Functions */

		/* Subscript */
		T &operator[](
				u32				idx
			) { return vec[idx]; }

		const T operator[](
				u32				idx
			) const { return vec[idx]; }

		/* Assignment */
		vec2<T> &operator=(
				const vec2<T>	&v
			);

		vec2<T> &operator=(
				const T			v
			);


		/* Comparison */
		bool operator==(
				const vec2<T>	&v
			) const;

		bool operator!=(
				const vec2<T>	&v
			) const;


		/* Addition */
		vec2<T> operator+(
				const vec2<T>	&v
			) const;

		vec2<T> &operator+=(
				const vec2<T>	&v
			);

		vec2<T> operator+(
				const T			v
			) const;

		vec2<T> &operator+=(
				const T			v
			);

		vec2<T> operator+() const;

/*		friend vec2<T> operator+(
				T				lv,
				vec2<T>			&rv
			);
			*/
		/* Subtraction */
		vec2<T> operator-(
				const vec2<T>	&v
			) const;

		vec2<T> &operator-=(
				const vec2<T>	&v
			);

		vec2<T> operator-(
				const T			v
			) const;

		vec2<T> &operator-=(
				const T			v
			);

		vec2<T> operator-() const;


		/* Scalar/element-wise multiplication */
		vec2<T> operator*(
				const vec2<T>	&v
			) const;

		vec2<T> &operator*=(
				const vec2<T>	&v
			);

		vec2<T> operator*(
				const mat2<T>	&v
			) const;

		vec2<T> &operator*=(
				const mat2<T>	&v
			);

		vec2<T> operator*(
				const T			v
			) const;

		vec2<T> &operator*=(
				const T			v
			);


		/* Dot product */
		T dot(
				const vec2<T>	&v
			) const;

		T operator^(
				const vec2<T>	&v
			) const;


		/* Division */
		vec2<T> operator/(
				const vec2<T>	&v
			) const;

		vec2<T> &operator/=(
				const vec2<T>	&v
			);

		vec2<T> operator/(
				const T			v
			) const;

		vec2<T> &operator/=(
				const T			v
			);


		/* Misc useful stuff */
		T minElement() const;

		T maxElement() const;

		void clamp(
				const T			min,
				const T			max
			);

		void clamp(
				const vec2<T>	&min,
				const vec2<T>	&max
			);

		void saturate();
		vec2<T> getSaturated() const;

		void normalize();
		vec2<T> getNormalized() const;

		T len() const;
		T length() const;

		T len2() const;
		T length2() const;
		T lengthSquared() const;

		vec2<T> reflection(
				const vec2<T>	&v
			) const;

		vec2<T> refraction(
				const vec2<T>	&v,
				const T			r
			) const;

		vec2<T> projectOn(
				const vec2<T>	&v
			) const;

		T projectionLengthOn(
				const vec2<T>	&v
			) const;

};

template<class T>
vec2<T> operator+(
		T				lv,
		vec2<T>			&rv
	)
{
	return rv + lv;
}

template<class T>
inline vec2<T> operator-(
		T				lv,
		vec2<T>			&rv
	)
{
	return - rv + lv;
}

template<class T>
inline vec2<T> operator*(
		T				lv,
		vec2<T>			&rv
	)
{
	return rv * lv;
}

template<class T>
inline vec2<T> operator/(
		T				lv,
		vec2<T>			&rv
	)
{
	vec4<T> d;
	d.x = lv / rv.x;
	d.y = lv / rv.y;
	return d;
}

template<class T>
vec2<T> operator+(
		T				lv,
		const vec2<T>	&rv
	)
{
	return rv + lv;
}

template<class T>
inline vec2<T> operator-(
		T				lv,
		const vec2<T>	&rv
	)
{
	return - rv + lv;
}

template<class T>
inline vec2<T> operator*(
		T				lv,
		const vec2<T>	&rv
	)
{
	return rv * lv;
}

template<class T>
inline vec2<T> operator/(
		T				lv,
		const vec2<T>	&rv
	)
{
	vec4<T> d;
	d.x = lv / rv.x;
	d.y = lv / rv.y;
	return d;
}


/*===========================================================[ 3D Vector ]====*/

template<class T> 
class vec3
{
	public:

		/* Members */
		union {
			struct {
				union {
					T		x;
					T		u;
					T		r;
				};
				union {
					T		y;
					T		v;
					T		g;
				};
				union {
					T		z;
					T		w;
					T		b;
				};
			};

			T				vec[3];
		};

		/* Constructors */
		vec3() {};

		vec3(
				const vec3<T>	&v
			) : x(v.x), y(v.y), z(v.z) {}

		vec3(
				T			nx,
				T			ny,
				T			nz
			) : x(nx), y(ny), z(nz) {}

		vec3(
				T			n
			) : x(n), y(n), z(n) {}

		vec3(
				T			nxyz[3]
			) : x(nxyz[0]), y(nxyz[1]), z(nxyz[2]) {}


		/* Functions */

		/* Subscript */
		T &operator[](
				u32				idx
			) { return vec[idx]; }

		const T operator[](
				u32				idx
			) const { return vec[idx]; }

		/* Assignment */
		vec3<T> &operator=(
				const vec3<T>	&v
			);

		vec3<T> &operator=(
				const T			v
			);

		/* Comparison */
		bool operator==(
				const vec3<T>	&v
			) const;

		bool operator!=(
				const vec3<T>	&v
			) const;


		/* Addition */
		vec3<T> operator+(
				const vec3<T>	&v
			) const;

		vec3<T> &operator+=(
				const vec3<T>	&v
			);

		vec3<T> operator+(
				const T			v
			) const;

		vec3<T> &operator+=(
				const T			v
			);

		vec3<T> operator+() const;


		/* Subtraction */
		vec3<T> operator-(
				const vec3<T>	&v
			) const;

		vec3<T> &operator-=(
				const vec3<T>	&v
			);

		vec3<T> operator-(
				const T			v
			) const;

		vec3<T> &operator-=(
				const T			v
			);

		vec3<T> operator-() const;


		/* Scalar/element-wise multiplication */
		vec3<T> operator*(
				const vec3<T>	&v
			) const;

		vec3<T> &operator*=(
				const vec3<T>	&v
			);

		vec3<T> operator*(
				const mat3<T>	&v
			) const;

		vec3<T> &operator*=(
				const mat3<T>	&v
			);

		vec3<T> operator*(
				const T			v
			) const;

		vec3<T> &operator*=(
				const T			v
			);


		/* Dot product */
		T dot(
				const vec3<T>	&v
			) const;

		T operator^(
				const vec3<T>	&v
			) const;


		/* Cross product */
		vec3<T> cross(
				const vec3<T>	&v
			) const;

		vec3<T> operator%(
				const vec3<T>	&v
			) const;


		/* Division */
		vec3<T> operator/(
				const vec3<T>	&v
			) const;

		vec3<T> &operator/=(
				const vec3<T>	&v
			);

		vec3<T> operator/(
				const T			v
			) const;

		vec3<T> &operator/=(
				const T			v
			);


		/* Misc useful stuff */
		T minElement() const;

		T maxElement() const;

		void clamp(
				const T			min,
				const T			max
			);

		void clamp(
				const vec3<T>	&min,
				const vec3<T>	&max
			);

		void saturate();
		vec3<T> getSaturated() const;
		
		void normalize();
		vec3<T> getNormalized() const;
	
		T len() const;
		T length() const;

		T len2() const;
		T length2() const;
		T lengthSquared() const;

		vec3<T> reflection(
				const vec3<T>	&v
			) const;

		vec3<T> refraction(
				const vec3<T>	&v,
				const T			r
			) const;

		vec3<T> projectOn(
				const vec3<T>	&v
			) const;

		f32 projectionLengthOn(
				const vec3<T>	&v
			) const;

		vec4<T> xyz0() const;

		vec4<T> xyz1() const;

		vec2<T> xy() const;

};

template<class T>
inline vec3<T> operator+(
		T				lv,
		vec3<T>			&rv
	)
{
	return rv + lv;
}

template<class T>
inline vec3<T> operator-(
		T				lv,
		vec3<T>			&rv
	)
{
	return - rv + lv;
}

template<class T>
inline vec3<T> operator*(
		T				lv,
		vec3<T>			&rv
	)
{
	return rv * lv;
}

template<class T>
inline vec3<T> operator/(
		T				lv,
		vec3<T>			&rv
	)
{
	vec4<T> d;
	d.x = lv / rv.x;
	d.y = lv / rv.y;
	d.z = lv / rv.z;
	return d;
}

template<class T>
inline vec3<T> operator+(
		T				lv,
		const vec3<T>	&rv
	)
{
	return rv + lv;
}

template<class T>
inline vec3<T> operator-(
		T				lv,
		const vec3<T>	&rv
	)
{
	return - rv + lv;
}

template<class T>
inline vec3<T> operator*(
		T				lv,
		const vec3<T>	&rv
	)
{
	return rv * lv;
}

template<class T>
inline vec3<T> operator/(
		T				lv,
		const vec3<T>	&rv
	)
{
	vec4<T> d;
	d.x = lv / rv.x;
	d.y = lv / rv.y;
	d.z = lv / rv.z;
	return d;
}

/*===========================================================[ 4D Vector ]====*/

template<class T> 
class vec4
{
	public:

		/* Members */
		union {
			struct {
				union {
					T		x;
					T		r;
				};
				union {
					T		y;
					T		g;
				};
				union {
					T		z;
					T		b;
				};
				union {
					T		w;
					T		a;
				};
			};

			T				vec[4];
		};

		/* Constructors */
		vec4() {}

		vec4(
				const vec4<T>	&v
			) : x(v.x), y(v.y), z(v.z), w(v.w) {}

		vec4(
				const vec3<T>	&v,
				T				nw
			) : x(v.x), y(v.y), z(v.z), w(nw) {}

		vec4(
				T			nx,
				T			ny,
				T			nz,
				T			nw
			) : x(nx), y(ny), z(nz), w(nw) {}


		/* Functions */

		/* Subscript */
		/* TODO: Fix! */
		T &operator[](
				u32				idx
			) { return vec[idx]; }

		const T operator[](
				u32				idx
			) const { return vec[idx]; }

		/* Assignment */
		vec4<T> &operator=(
				const vec4<T>	&v
			);

		vec4<T> &operator=(
				const T			v
			);

		/* Comparison */
		bool operator==(
				const vec4<T>	&v
			) const;

		bool operator!=(
				const vec4<T>	&v
			) const;


		/* Addition */
		vec4<T> operator+(
				const vec4<T>	&v
			) const;

		vec4<T> &operator+=(
				const vec4<T>	&v
			);

		vec4<T> operator+(
				const T			v
			) const;

		vec4<T> &operator+=(
				const T			v
			);

		vec4<T> operator+() const;


		/* Subtraction */
		vec4<T> operator-(
				const vec4<T>	&v
			) const;

		vec4<T> &operator-=(
				const vec4<T>	&v
			);

		vec4<T> operator-(
				const T			v
			) const;

		vec4<T> &operator-=(
				const T			v
			);

		vec4<T> operator-() const;


		/* Scalar/element-wise multiplication */
		vec4<T> operator*(
				const vec4<T>	&v
			) const;

		vec4<T> &operator*=(
				const vec4<T>	&v
			);

		vec4<T> operator*(
				const mat4<T>	&v
			) const;

		vec4<T> &operator*=(
				const mat4<T>	&v
			);

		vec4<T> operator*(
				const T			v
			) const;

		vec4<T> &operator*=(
				const T			v
			);


		/* Dot product */
		T dot(
				const vec4<T>	&v
			) const;

		T operator^(
				const vec4<T>	&v
			) const;


		/* Division */
		vec4<T> operator/(
				const vec4<T>	&v
			) const;

		vec4<T> &operator/=(
				const vec4<T>	&v
			);

		vec4<T> operator/(
				const T			v
			) const;

		vec4<T> &operator/=(
				const T			v
			);


		/* Misc useful stuff */
		T minElement() const;

		T maxElement() const;

		void clamp(
				const T			min,
				const T			max
			);

		void clamp(
				const vec4<T>	&min,
				const vec4<T>	&max
			);

		void saturate();
		vec4<T> getSaturated() const;
		
		void normalize();
		vec4<T> getNormalized() const;
	
		T len() const;
		T length() const;

		T len2() const;
		T length2() const;
		T lengthSquared() const;

		vec4<T> reflection(
				const vec4<T>	&v
			) const;

		vec4<T> refraction(
				const vec4<T>	&v,
				const T			r
			) const;

		vec4<T> projectOn(
				const vec4<T>	&v
			) const;

		f32 projectionLengthOn(
				const vec4<T>	&v
			) const;

		vec3<T> xyz() const;

		vec2<T> xy() const;

};


template<class T>
inline vec4<T> operator+(
		T				lv,
		vec4<T>			&rv
	)
{
	return rv + lv;
}

template<class T>
inline vec4<T> operator-(
		T				lv,
		vec4<T>			&rv
	)
{
	return - rv + lv;
}

template<class T>
inline vec4<T> operator*(
		T				lv,
		vec4<T>			&rv
	)
{
	return rv * lv;
}

template<class T>
inline vec4<T> operator/(
		T				lv,
		vec4<T>			&rv
	)
{
	vec4<T> d;
	d.x = lv / rv.x;
	d.y = lv / rv.y;
	d.z = lv / rv.z;
	d.w = lv / rv.w;
	return d;
}

template<class T>
inline vec4<T> operator+(
		T				lv,
		const vec4<T>	&rv
	)
{
	return rv + lv;
}

template<class T>
inline vec4<T> operator-(
		T				lv,
		const vec4<T>	&rv
	)
{
	return - rv + lv;
}

template<class T>
inline vec4<T> operator*(
		T				lv,
		const vec4<T>	&rv
	)
{
	return rv * lv;
}

template<class T>
inline vec4<T> operator/(
		T				lv,
		const vec4<T>	&rv
	)
{
	vec4<T> d;
	d.x = lv / rv.x;
	d.y = lv / rv.y;
	d.z = lv / rv.z;
	d.w = lv / rv.w;
	return d;
}

/*==========================================================[ 2x2 Matrix ]====*/

template<class T>
class mat2
{
	public:
		friend class vec2<T>;

		/* Members */
		union {
			struct {
				T			a;
				T			b;

				T			c;
				T			d;
			};

			T				vec[4];
			T				mat[2][2];
		};

	
		/* Constructors */
		mat2() : a(1), b(0), c(0), d(1) {}

		mat2(
				const mat2<T>	&v
			) : a(v.a), b(v.b), c(v.c), d(v.d) {}

		mat2(
				T				na,
				T				nb,
				T				nc,
				T				nd
			) : a(na), b(nb), c(nc), d(nd) {}

		/* Functions */

		/* Assignment */
		mat2<T> &operator=(
				const mat2<T>	&v
			);


		/* Comparison */
		bool operator==(
				const mat2<T>	&v
			) const;

		bool operator!=(
				const mat2<T>	&v
			) const;

		bool isIdentity() const;


		/* Addition */
		mat2<T> operator+(
				const mat2<T>	&v
			) const;

		mat2<T> &operator+=(
				const mat2<T>	&v
			);

		mat2<T> operator+(
				const T			v
			) const;

		mat2<T> &operator+=(
				const T			v
			);

		mat2<T> operator+() const;


		/* Subtraction */
		mat2<T> operator-(
				const mat2<T>	&v
			) const;

		mat2<T> &operator-=(
				const mat2<T>	&v
			);

		mat2<T> operator-(
				const T			v
			) const;

		mat2<T> &operator-=(
				const T			v
			);

		mat2<T> operator-() const;


		/* Inner product */
		T iprod(
				const mat2<T>	&v,
				const u32		row,
				const u32		col
			) const;


		/* Multiplication */
		mat2<T> operator*(
				const mat2<T>	&v
			) const;

		mat2<T> &operator*=(
				const mat2<T>	&v
			);

		mat2<T> operator*(
				const T			v
			) const;

		mat2<T> &operator*=(
				const T			v
			);

		vec2<T> operator*(
				const vec2<T>	&v
			) const;


		/* Division */
		mat2<T> operator/(
				const T			v
			) const;

		mat2<T> &operator/=(
				const T			v
			);


		/* Element-wise multiplication */
		mat2<T> elemMul(
				const mat2<T>	&v
			) const;


		/* Division with scalar */
		mat2<T> operator/(
				const mat2<T>	&v
			) const;

		mat2<T> &operator/=(
				const mat2<T>	&v
			);


		/* Misc matrix operations */
		void identity();

		void transpose();

		T det() const;
		T determinant() const;

		void inv();
		void inverse();

		T trace() const;

		vec2<T> eigenvalues() const;

		mat2<T> eigenvectors(
				const vec2<T>	&eig
			) const;

		vec2<T> getRow(
				u32		row
			) const;

		void setRow(
				u32		row,
				vec2<T>	vec
			);

		vec2<T> getCol(
				u32		col
			) const;

		void setCol(
				u32		col,
				vec2<T>	vec
			);

		vec2<T> solveSystem(
				vec2<T>	right_side
			) const;

		vec2<T> up() const;
		vec2<T> down() const;
		vec2<T> left() const;
		vec2<T> right() const;
};

template<class T>
inline mat2<T> operator+(
		T				lv,
		mat2<T>			&rv
	)
{
	return rv + lv;
}

template<class T>
inline mat2<T> operator-(
		T				lv,
		mat2<T>			&rv
	)
{
	return - rv + lv;
}

template<class T>
inline mat2<T> operator*(
		T				lv,
		mat2<T>			&rv
	)
{
	return rv * lv;
}

/*==========================================================[ 3x3 Matrix ]====*/

template<class T>
class mat3
{
	public:
		friend class vec3<T>;

		/* Members */
		union {
			struct {
				T			a;
				T			b;
				T			c;
				
				T			d;
				T			e;
				T			f;

				T			g;
				T			h;
				T			i;
			};


			T				vec[9];
			T				mat[3][3];
		};

	
		/* Constructors */
		mat3() : a(1), b(0), c(0),
				 d(0), e(1), f(0),
				 g(0), h(0), i(1) {}

		mat3(
				const mat3<T>	&v
			) : a(v.a), b(v.b), c(v.c),
				d(v.d), e(v.e), f(v.f),
				g(v.g), h(v.h), i(v.i) {}

		mat3(
				T				na,
				T				nb,
				T				nc,

				T				nd,
				T				ne,
				T				nf,

				T				ng,
				T				nh,
				T				ni
			) : a(na), b(nb), c(nc),
				d(nd), e(ne), f(nf),
				g(ng), h(nh), i(ni) {}

		mat3(
				const vec3<T>	&row0,
				const vec3<T>	&row1,
				const vec3<T>	&row2
			) : a(row0.x), b(row0.y), c(row0.z),
				d(row1.x), e(row1.y), f(row1.z),
				g(row2.x), h(row2.y), i(row2.z) {}

		/* Functions */

		/* Assignment */
		mat3<T> &operator=(
				const mat3<T>	&v
			);


		/* Comparison */
		bool operator==(
				const mat3<T>	&v
			) const;

		bool operator!=(
				const mat3<T>	&v
			) const;

		bool isIdentity() const;


		/* Addition */
		mat3<T> operator+(
				const mat3<T>	&v
			) const;

		mat3<T> &operator+=(
				const mat3<T>	&v
			);

		mat3<T> operator+(
				const T			v
			) const;

		mat3<T> &operator+=(
				const T			v
			);

		mat3<T> operator+() const;


		/* Subtraction */
		mat3<T> operator-(
				const mat3<T>	&v
			) const;

		mat3<T> &operator-=(
				const mat3<T>	&v
			);

		mat3<T> operator-(
				const T			v
			) const;

		mat3<T> &operator-=(
				const T			v
			);

		mat3<T> operator-() const;


		/* Inner product */
		T iprod(
				const mat3<T>	&v,
				const u32		row,
				const u32		col
			) const;


		/* Multiplication */
		mat3<T> operator*(
				const mat3<T>	&v
			) const;

		mat3<T> &operator*=(
				const mat3<T>	&v
			);

		mat3<T> operator*(
				const T			v
			) const;

		mat3<T> &operator*=(
				const T			v
			);

		vec3<T> operator*(
				const vec3<T>	&v
			) const;


		/* Division */
		mat3<T> operator/(
				const T			v
			) const;

		mat3<T> &operator/=(
				const T			v
			);


		/* Element-wise multiplication */
		mat3<T> elemMul(
				const mat3<T>	&v
			) const;


		/* Division with scalar */
		mat3<T> operator/(
				const mat3<T>	&v
			) const;

		mat3<T> &operator/=(
				const mat3<T>	&v
			);


		/* Misc matrix operations */
		void identity();

		void transpose();

		T det() const;
		T determinant() const;

		void inv();
		void inverse();

		T trace() const;

		vec3<T> getRow(
				u32		row
			) const;

		void setRow(
				u32		row,
				vec3<T>	vec
			);

		vec3<T> getCol(
				u32		col
			) const;

		void setCol(
				u32		col,
				vec3<T>	vec
			);

		vec3<T> up() const;
		vec3<T> down() const;
		vec3<T> left() const;
		vec3<T> right() const;
		vec3<T> front() const;
		vec3<T> back() const;
};

template<class T>
inline mat3<T> operator+(
		T				lv,
		mat3<T>			&rv
	)
{
	return rv + lv;
}

template<class T>
inline mat3<T> operator-(
		T				lv,
		mat3<T>			&rv
	)
{
	return - rv + lv;
}

template<class T>
inline mat3<T> operator*(
		T				lv,
		mat3<T>			&rv
	)
{
	return rv * lv;
}

/*==========================================================[ 4x4 Matrix ]====*/

template<class T>
class mat4
{
	public:
		friend class vec4<T>;

		/* Members */
		union {
			struct {
				T			a;
				T			b;
				T			c;
				T			d;

				T			e;
				T			f;
				T			g;
				T			h;

				T			i;
				T			j;
				T			k;
				T			l;

				T			m;
				T			n;
				T			o;
				T			p;
			};
			struct {
				T			R00;
				T			R10;
				T			R20;
				T			T0;

				T			R01;
				T			R11;
				T			R21;
				T			T1;

				T			R02;
				T			R12;
				T			R22;
				T			T2;

				T			N0;
				T			N1;
				T			N2;
				T			N3;
			};

			T				vec[16];
			T				mat[4][4];

		};

	
		/* Constructors */
		mat4() : a(1), b(0), c(0), d(0),
				 e(0), f(1), g(0), h(0),
				 i(0), j(0), k(1), l(0),
				 m(0), n(0), o(0), p(1) {}

		mat4(
				const mat4<T>	&v
			) : a(v.a), b(v.b), c(v.c), d(v.d),
				e(v.e), f(v.f), g(v.g), h(v.h),
				i(v.i), j(v.j), k(v.k), l(v.l),
				m(v.m), n(v.n), o(v.o), p(v.p) {}

		mat4(
				T				na,
				T				nb,
				T				nc,
				T				nd,

				T				ne,
				T				nf,
				T				ng,
				T				nh,

				T				ni,
				T				nj,
				T				nk,
				T				nl,

				T				nm,
				T				nn,
				T				no,
				T				np
			) : a(na), b(nb), c(nc), d(nd),
				e(ne), f(nf), g(ng), h(nh),
				i(ni), j(nj), k(nk), l(nl),
				m(nm), n(nn), o(no), p(np) {}

		/* Functions */

		/* Assignment */
		mat4<T> &operator=(
				const mat4<T>	&v
			);


		/* Comparison */
		bool operator==(
				const mat4<T>	&v
			) const;

		bool operator!=(
				const mat4<T>	&v
			) const;

		bool isIdentity() const;


		/* Addition */
		mat4<T> operator+(
				const mat4<T>	&v
			) const;

		mat4<T> &operator+=(
				const mat4<T>	&v
			);

		mat4<T> operator+(
				const T			v
			) const;

		mat4<T> &operator+=(
				const T			v
			);

		mat4<T> operator+() const;


		/* Subtraction */
		mat4<T> operator-(
				const mat4<T>	&v
			) const;

		mat4<T> &operator-=(
				const mat4<T>	&v
			);

		mat4<T> operator-(
				const T			v
			) const;

		mat4<T> &operator-=(
				const T			v
			);

		mat4<T> operator-() const;


		/* Inner product */
		T iprod(
				const mat4<T>	&v,
				const u32		row,
				const u32		col
			) const;


		/* Multiplication */
		mat4<T> operator*(
				const mat4<T>	&v
			) const;

		mat4<T> &operator*=(
				const mat4<T>	&v
			);

		mat4<T> operator*(
				const T			v
			) const;

		mat4<T> &operator*=(
				const T			v
			);

		vec4<T> operator*(
				const vec4<T>	&v
			) const;

		mat4<T> affineMul(
				const mat4<T>	&v
			) const;

		vec4<T> affineMul(
				const vec4<T>	&v
			) const;

		vec3<T> affineMul(
				const vec3<T>	&v
			) const;

		/* Division */
		mat4<T> operator/(
				const T			v
			) const;

		mat4<T> &operator/=(
				const T			v
			);


		/* Element-wise multiplication */
		mat4<T> elemMul(
				const mat4<T>	&v
			) const;


		/* Division with scalar */
		mat4<T> operator/(
				const mat4<T>	&v
			) const;

		mat4<T> &operator/=(
				const mat4<T>	&v
			);


		/* Misc matrix operations */
		void identity();

		void transpose();

		T det() const;
		T determinant() const;

		void inv();
		void inverse();

		T trace() const;

		void print();

		mat3<T> rotationMatrix() const;

		vec4<T> getRow(
				u32		row
			) const;

		void setRow(
				u32		row,
				vec4<T>	vec
			);

		vec4<T> getCol(
				u32		col
			) const;

		void setCol(
				u32		col,
				vec4<T>	vec
			);

	private:
		T det2(
				T n0,
				T n1,
				T n2,
				T n3
			) const;

		T affineiprod0(
				const mat4<T>	&v,
				const u32		row,
				const u32		col
			) const;

		T affineiprod1(
				const mat4<T>	&v,
				const u32		row,
				const u32		col
			) const;

};

template<class T>
inline mat4<T> operator+(
		T				lv,
		mat4<T>			&rv
	)
{
	return rv + lv;
}

template<class T>
inline mat4<T> operator-(
		T				lv,
		mat4<T>			&rv
	)
{
	return - rv + lv;
}

template<class T>
inline mat4<T> operator*(
		T				lv,
		mat4<T>			&rv
	)
{
	return rv * lv;
}

/*===================================================[ Template uglyness ]====*/

typedef vec2<float>			vec2f;
typedef vec2<double>		vec2d;

typedef vec3<float>			vec3f;
typedef vec3<double>		vec3d;

typedef vec4<float>			vec4f;
typedef vec4<double>		vec4d;


typedef mat2<float>			mat2f;
typedef mat2<double>		mat2d;

typedef mat3<float>			mat3f;
typedef mat3<double>		mat3d;

typedef mat4<float>			mat4f;
typedef mat4<double>		mat4d;


/*================================================[ Additional constants ]====*/

static const mat4f ident4f(1.0f, 0.0f, 0.0f, 0.0f,
						   0.0f, 1.0f, 0.0f, 0.0f,
						   0.0f, 0.0f, 1.0f, 0.0f,
						   0.0f, 0.0f, 0.0f, 1.0f);

static const mat3f ident3f(1.0f, 0.0f, 0.0f,
						   0.0f, 1.0f, 0.0f,
						   0.0f, 0.0f, 1.0f);

#define RC_VMATH
#include "VectorMath.cpp"
#undef RC_VMATH

#endif /* RC_VECTORMATH_H */

