#include "Example.h"
#include "RenderChimp.h"

World			*world;
Geometry		*bunny;
Camera			*camera;
Group			*camera_pivot;
Light			*light;
Group			*light_pivot;
ShaderProgram	*shader;
Texture			*tex;
vec2f			prev_pos;
vec2f			camera_rotation;
vec2f			mouse_prev_pos;
f32				starttime;

ShaderProgram	*sBufferShader;
RenderTarget	*depthBuffer;
Camera			*depthCamera;

void RCInit() {
	u32 x, y;
	world = SceneGraph::createWorld("World");
	Platform::getDisplaySize(&x, &y);
	
	camera = SceneGraph::createCamera("Camera");
	camera->setPerspectiveProjection(1.2f, 4.0f / 3.0f, 1.0f, 1000.0f);
	camera->translate(0.0f, 0.0f, 10.0f);
	world->setActiveCamera(camera);
	
	camera_pivot = SceneGraph::createGroup("Camera Pivot");
	camera_pivot->attachChild(camera);
	world->attachChild(camera_pivot);
	
	depthBuffer = SceneGraph::createRenderTarget("DepthRT", 512, 512, 1, true, false,TEXTURE_FILTER_NEAREST);
	Renderer::setRenderTarget(depthBuffer);
	Renderer::clearColor();
	Renderer::clearDepth();
	
	sBufferShader = SceneGraph::createShaderProgram("DBufferSP", "DepthBuffer", 0);
	RenderState *state = sBufferShader->getRenderState();
	state->enableDepthTest(CMP_LESS);
	state->enableCulling(CULL_FRONT);
	state->enableBlend(SB_ONE, DB_ZERO);
	
	light = SceneGraph::createLight("Light");
	light->setColor(1.0f, 1.0f, 1.0f);
	light->setTranslate(0.0f, 0.0f, 20.0f);
	Geometry *lightBulb = SceneGraph::createGeometry("Light Bulb", "scenes/sphere.obj");
	ShaderProgram *lightBulbShader = SceneGraph::createShaderProgram("bulb_shader", "LightSource", 0);
	lightBulb->setShaderProgram(lightBulbShader);
	light->attachChild(lightBulb);

	depthCamera = SceneGraph::createCamera(0);
	depthCamera->setPerspectiveProjection(0.785398f*2.f, 1.f, 0.1f, 500);
	light->attachChild(depthCamera);

	light_pivot = SceneGraph::createGroup("Light Pivot");
	light_pivot->attachChild(light);
	world->attachChild(light_pivot);
	
	shader = SceneGraph::createShaderProgram("BunnyShader", "Subsurface", 0);
	tex = SceneGraph::createTexture("Tex", "textures/meat.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	bunny = SceneGraph::createGeometry("Bunny", "models/bunny.obj", 0);
	bunny->setScale(2.0f, 2.0f, 2.0f);
	bunny->setShaderProgram(shader, true);
	bunny->setTexture("DiffuseTex", tex);
	bunny->setVector("LightColor", new vec4f(light->getColor(), 1.0f), 4, UNIFORM_FLOAT32);
	bunny->setVector("SpecularColor", new vec4f(0.2f, 0.2f, 0.2f, 1.0f), 4, UNIFORM_FLOAT32);
	bunny->setVector("ScatterColor", new vec4f(0.15f, 0.0f, 0.0f, 1.0f), 4, UNIFORM_FLOAT32);
	bunny->setScalar("ScatterWidth", 0.1f, UNIFORM_FLOAT32);
	bunny->setScalar("Wrap", 5.0f, UNIFORM_FLOAT32);
	bunny->setScalar("Shininess", 40.0f, UNIFORM_FLOAT32);
	bunny->setTexture("DepthBuffer", depthBuffer->getTexture(0));
	world->attachChild(bunny);
	
	prev_pos = vec2f(0.0f, 0.0f);
	camera_rotation = vec2f(0.0f, 0.0f);

	Console::log("Bunny Bunny");

	Renderer::setClearColor(vec4f(0.2f, 0.3f, 0.4f, 1.0f));
	Renderer::setClearDepth(1.0f);
}

void handleMouse() {
	bool *mouse = Platform::getMouseButtonState();
	vec2f mouse_pos = Platform::getMousePosition();
	if (mouse[RC_MOUSE_LEFT]) {
		camera_rotation.x = -mouse_pos.x * 2.0f;
		camera_rotation.y = mouse_pos.y * 2.0f;
	} else if (mouse[RC_MOUSE_RIGHT]) {
		vec2f diff = mouse_pos - mouse_prev_pos;
		camera_rotation.x -= diff.x * 2.0f;
		camera_rotation.y += diff.y * 2.0f;
	}
	camera_pivot->setRotateX(camera_rotation.y);
	camera_pivot->rotateY(camera_rotation.x);
}

void handleKeyboard() {
	bool *keys = Platform::getKeyState();
	f32 move = 0.0f, strafe = 0.0f;
	if (keys[RC_KEY_W])	move += 0.1f;
	if (keys[RC_KEY_S])	move -= 0.1f;
	if (keys[RC_KEY_A])	strafe -= 0.1f;
	if (keys[RC_KEY_D])	strafe += 0.1f;
	camera->translate(camera->getLocalFront() * move);
	camera->translate(camera->getLocalRight() * strafe);
}

u32 RCUpdate() {
	handleMouse();
	handleKeyboard();

	light_pivot->rotateY(1 * Platform::getFrameTimeStep());
	bunny->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	
	Renderer::setRenderTarget(depthBuffer);
	Renderer::clearColor();
	Renderer::clearDepth();
	Renderer::setCameraMatrices(depthCamera);
	// TODO: You need to use the camera to set the correct camera matrices to the renderer
	Renderer::render(geometryList, sBufferShader);
	spotLightShader->setValue("shadowViewProjection", shadowCamera->getViewProjectionMatrix());
	light_list[i]->detachChild(shadowCamera); 
		
	Renderer::setCameraMatrices(camera);
	Renderer::setRenderTarget(lightBuffer);
	Renderer::render(*lightBounds, spotLightShader);

	Renderer::clearColor();
	Renderer::clearDepth();
	world->renderAll();

	return 0;
}

void RCDestroy() {
	SceneGraph::deleteNode(world);
}



