
#ifndef RC_INTERPOLATION_H
#define RC_INTERPOLATION_H


/*
	Linear interpolation
		
	p(x) = | 1	x | *	|	1	0	| *	|	p0	|
						|	-1	1	|	|	p1	|

	CJG sep 2010 - created
*/
vec3f evalLERP(vec3f &p0, vec3f &p1, const f32 x);

/*
	Catmull-Rom spline interpolation

								|	0	1	0		0	|	|	p0	|
	Q(x) = | 1	x	x^2	x^3	| *	|	-t	0	t		0	| * |	p1	|
								|	2t	t-3	3-2t	-t	|	|	p2	|
								|	-t	2-t	t-2		t	|	|	p3	|

	CJG aug 2010 - created
*/
vec3f evalCatmullRom(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 t, const f32 x);


/*
	Catmull-Rom tangent

			dQ(x)
	T(x) =	-----
			 dx
	 
	 CJG aug 2010 - created
*/
vec3f evalCatmullRomTangent(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 t, const f32 x);


/*
	B�zier spline

			|	1	-3	3	-1	|		|	1	|		|	p0	|
	B(x) =	|	0	3	-6	3	|	*	|	x	|	*	|	p1	|
			|	0	0	3	-3	|		|	x^2	|		|	p2	|
			|	0	0	0	1	|		|	x^3	|		|	p3	|

	CJG aug 2010 - created
*/
vec3f evalBezier(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 x);


/*
	Bezier tangent

			dB(x)
	T(x) =	-----
			 dx

	CJG aug 2010 - created

*/
vec3f evalBezierTangent(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 x);


#endif /* RC_INTERPOLATION_H */

