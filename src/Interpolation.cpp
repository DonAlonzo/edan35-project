

#include "RenderChimp.h"


/*
	Linear interpolation
		
	p(x) = |1	x| *|	1	0	| *	|	p0	|
					|	-1	1	|	|	p1	|

	CJG sep 2010 - created
*/
vec3f evalLERP(vec3f &p0, vec3f &p1, const f32 x)
{
	return	p0 * ( 1.0f - x ) + 
			p1 * ( x );
}


/*
	Catmull-Rom spline interpolation

								|	0	1	0		0	|	|	p0	|
	Q(x) = | 1	x	x^2	x^3	| *	|	-t	0	t		0	| * |	p1	|
								|	2t	t-3	3-2t	-t	|	|	p2	|
								|	-t	2-t	t-2		t	|	|	p3	|

	CJG aug 2010 - created
*/
vec3f evalCatmullRom(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 t, const f32 x)
{
	float	x2 = x*x,
			x3 = x2 * x;

	vec3f Q = (	p0 * ( -t*x + 2.0f*t*x2 - t*x3 ) +
				p1 * ( 1.0f + (t-3.0f)*x2 + (2.0f-t)*x3 ) +
				p2 * ( t*x + (3.0f-2.0f*t)*x2 + (t-2.0f)*x3 ) +
				p3 * ( -t*x2 + t*x3 ) );

	return	Q;
}


/*
	Catmull-Rom tangent

			dQ(x)
	T(x) =	-----
			 dx
	 
	 CJG aug 2010 - created
*/
vec3f evalCatmullRomTangent(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 t, const f32 x)
{
	float	x2 = x*x;

	vec3f T = (	p0 * ( -t + 4.0f*t*x - 3.0f*t*x2 ) +
				p1 * ( 2.0f*(t-3.0f)*x + 3.0f*(2.0f-t)*x2 ) +
				p2 * ( t + 2.0f*(3.0f-2.0f*t)*x + 3.0f*(t-2.0f)*x2 ) +
				p3 * ( -2.0f*t*x + 3.0f*t*x2 ) );

	return	T;
}


/*
	B�zier spline interpolation

			|	1	-3	3	-1	|		|	1	|		|	p0	|
	B(x) =	|	0	3	-6	3	|	*	|	x	|	*	|	p1	|
			|	0	0	3	-3	|		|	x^2	|		|	p2	|
			|	0	0	0	1	|		|	x^3	|		|	p3	|

	CJG aug 2010 - created
*/
vec3f evalBezier(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 x)
{
	float	x2 = x*x,
			x3 = x2 * x;

	vec3f B =	p0 * (1.0f - 3.0f*x + 3.0f*x2 - x3) +
				p1 * (3.0f*x - 6.0f*x2 + 3.0f*x3) +
				p2 * (3.0f*x2 - 3.0f*x3) +
				p3 * (x3);

	return	B;
}


/*
	Bezier tangent

			dB(x)
	T(x) =	-----
			 dx

	CJG aug 2010 - created

*/
vec3f evalBezierTangent(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 x)
{
	float	x2 = x*x;

	vec3f T =	p0 * (-3.0f + 6.0f*x - 3.0f*x2) +
				p1 * (3.0f - 12.0f*x + 9.0f*x2) +
				p2 * (6.0f*x - 9.0f*x2) +
				p3 * (3.0f*x2);

	return	T;
}


