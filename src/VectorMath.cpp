
#ifdef RC_VMATH


#include "RenderChimp.h"
#include <cmath>

/* Functions */

/* Assignment */
template<class T>
inline vec2<T> &vec2<T>::operator=(
		const vec2<T>	&v
	)
{
	x = v.x;
	y = v.y;

	return *this;
}

template<class T>
inline vec2<T> &vec2<T>::operator=(
		const T			v
	)
{
	x = v;
	y = v;

	return *this;
}


/* Comparison */
template<class T>
inline bool vec2<T>::operator==(
		const vec2<T>	&v
	) const
{
	return x == v.x &&
		y == v.y;
}

template<class T>
inline bool vec2<T>::operator!=(
		const vec2<T>	&v
	) const
{
	return x != v.x ||
		y != v.y;
}


/* Addition */
template<class T> 
inline vec2<T> vec2<T>::operator+(
		const vec2<T>	&v
	) const
{
	return vec2<T>(x + v.x, y + v.y);
}

template<class T> 
inline vec2<T> &vec2<T>::operator+=(
		const vec2<T>	&v
	)
{
	x += v.x;
	y += v.y;
	return *this;
}

template<class T> 
inline vec2<T> vec2<T>::operator+(
		const T			v
	) const
{
	return vec2<T>(x + v, y + v);
}

template<class T> 
inline vec2<T> &vec2<T>::operator+=(
		const T			v
	)
{
	x += v;
	y += v;
	return *this;
}

template<class T> 
inline vec2<T> vec2<T>::operator+() const
{
	return vec2<T>(x, y);
}

/*
template<class T>
vec2<T> operator+(
		T				lv,
		vec2<T>			&rv
	)
{
	return rv + lv;
}
*/

/* Subtract */
template<class T> 
inline vec2<T> vec2<T>::operator-(
		const vec2<T>	&v
	) const
{
	return vec2<T>(x - v.x, y - v.y);
}

template<class T> 
inline vec2<T> &vec2<T>::operator-=(
		const vec2<T>	&v
	)
{
	x -= v.x;
	y -= v.y;
	return *this;
}

template<class T> 
inline vec2<T> vec2<T>::operator-(
		const T			v
	) const
{
	return vec2<T>(x - v, y - v);
}

template<class T> 
inline vec2<T> &vec2<T>::operator-=(
		const T			v
	)
{
	x -= v;
	y -= v;
	return *this;
}

template<class T> 
inline vec2<T> vec2<T>::operator-() const
{
	return vec2<T>(-x, -y);
}


/* Scalar/element-wise multiplication */
template<class T> 
inline vec2<T> vec2<T>::operator*(
		const vec2<T>	&v
	) const
{
	return vec2<T>(x * v.x, y * v.y);
}

template<class T> 
inline vec2<T> &vec2<T>::operator*=(
		const vec2<T>	&v
	)
{
	x *= v.x;
	y *= v.y;
	return *this;
}

template<class T> 
inline vec2<T> vec2<T>::operator*(
		const mat2<T>	&v
	) const
{
	return vec2f(v * (*this));
}

template<class T> 
inline vec2<T> &vec2<T>::operator*=(
		const mat2<T>	&v
	)
{
	vec2f nv = vec2f(v * (*this));
	x = nv.x;
	y = nv.y;
	return *this;
}

template<class T> 
inline vec2<T> vec2<T>::operator*(
		const T			v
	) const
{
	return vec2<T>(x * v, y * v);
}

template<class T>
inline vec2<T> &vec2<T>::operator*=(
		const T			v
	)
{
	x *= v;
	y *= v;
	return *this;
}


/* Dot product */
template<class T>
inline T vec2<T>::dot(
		const vec2<T>	&v
	) const
{
	return x * v.x + y * v.y;
}

template<class T>
inline T vec2<T>::operator^(
		const vec2<T>	&v
	) const
{
	return dot(v);
}

/* Division */
template<class T>
inline vec2<T> vec2<T>::operator/(
		const vec2<T>	&v
	) const
{
	return vec2<T>(x / v.x, y / v.y);
}

template<class T>
inline vec2<T> &vec2<T>::operator/=(
		const vec2<T>	&v
	)
{
	x /= v.x;
	y /= v.y;
	return *this;
}

template<class T>
inline vec2<T> vec2<T>::operator/(
		const T			v
	) const
{
	T t = ((T) 1) / v;
	return vec2<T>(x * t, y * t);
}

template<class T>
inline vec2<T> &vec2<T>::operator/=(
		const T			v
	)
{
	T t = ((T) 1) / v;
	x *= t;
	y *= t;
	return *this;
}


/* Misc useful stuff */
template<class T>
inline T vec2<T>::minElement() const
{
	return x < y ? x : y;
}

template<class T>
inline T vec2<T>::maxElement() const
{
	return x > y ? x : y;
}

template<class T>
inline void vec2<T>::clamp(
		const T			min,
		const T			max
	)
{
	x = sclamp(x, min, max);
	y = sclamp(y, min, max);
}

template<class T>
inline void vec2<T>::clamp(
		const vec2<T>	&min,
		const vec2<T>	&max
	)
{
	x = sclamp(x, min.x, max.x);
	y = sclamp(y, min.y, max.y);
}

template<class T>
inline void vec2<T>::saturate()
{
	x = ssaturate(x);
	y = ssaturate(y);
}

template<class T>
inline vec2<T> vec2<T>::getSaturated() const
{
	vec2<T> r;
	r.x = ssaturate(x);
	r.y = ssaturate(y);
	return r;
}

template<class T>
inline void vec2<T>::normalize()
{
	T t = ((T) 1) / length();
	x *= t;
	y *= t;
}

template<class T>
inline vec2<T> vec2<T>::getNormalized() const
{
	vec2<T> r;
	T t = ((T) 1) / length();
	
	r.x = x * t;
	r.y = y * t;
	
	return r;
}

template<class T>
inline T vec2<T>::len() const
{
	return sqrt(x * x + y * y);
}

template<class T>
inline T vec2<T>::length() const
{
	return len();
}

template<class T>
inline T vec2<T>::len2() const
{
	return x * x + y * y;
}

template<class T>
inline T vec2<T>::length2() const
{
	return len2();
}

template<class T>
inline T vec2<T>::lengthSquared() const
{
	return len2();
}

template<class T>
inline vec2<T> vec2<T>::reflection(
		const vec2<T>	&v
	) const
{
	T nv2 = dot(v) * ((T) 2);
	return vec2<T>(x + v.x, y + v.y) * nv2;
}

template<class T>
inline vec2<T> vec2<T>::refraction(
		const vec2<T>	&v,
		const T			r
	) const
{
	T w, k, wk;
	w = -dot(v) * r;
	k = sqrt(((T) 1) + (w - r) * (w + r));
	wk = w - k;
	return vec2<T>(r * x + wk * v.x,
				   r * y + wk * v.y);
}

template<class T>
T vec2<T>::projectionLengthOn(
		const vec2<T>	&v
	) const
{
	return dot(v) / v.len();
}

template<class T>
vec2<T> vec2<T>::projectOn(
		const vec2<T>	&v
	) const
{
	return v * (dot(v) / (v.dot(v)));
}







/* Functions */

/* Assignment */
template<class T>
inline vec3<T> &vec3<T>::operator=(
		const vec3<T>	&v
	)
{
	x = v.x;
	y = v.y;
	z = v.z;

	return *this;
}

template<class T>
inline vec3<T> &vec3<T>::operator=(
		const T			v
	)
{
	x = v;
	y = v;
	z = v;

	return *this;
}


/* Comparison */
template<class T>
inline bool vec3<T>::operator==(
		const vec3<T>	&v
	) const
{
	return x == v.x &&
		y == v.y &&
		z == v.z;
}

template<class T>
inline bool vec3<T>::operator!=(
		const vec3<T>	&v
	) const
{
	return x != v.x ||
		y != v.y ||
		z != v.z;
}


/* Addition */
template<class T> 
inline vec3<T> vec3<T>::operator+(
		const vec3<T>	&v
	) const
{
	return vec3<T>(x + v.x, y + v.y, z + v.z);
}

template<class T> 
inline vec3<T> &vec3<T>::operator+=(
		const vec3<T>	&v
	)
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

template<class T> 
inline vec3<T> vec3<T>::operator+(
		const T			v
	) const
{
	return vec3<T>(x + v, y + v, z + v);
}

template<class T> 
inline vec3<T> &vec3<T>::operator+=(
		const T			v
	)
{
	x += v;
	y += v;
	z += v;
	return *this;
}

template<class T> 
inline vec3<T> vec3<T>::operator+() const
{
	return vec3<T>(x, y, z);
}


/* Subtract */
template<class T> 
inline vec3<T> vec3<T>::operator-(
		const vec3<T>	&v
	) const
{
	return vec3<T>(x - v.x, y - v.y, z - v.z);
}

template<class T> 
inline vec3<T> &vec3<T>::operator-=(
		const vec3<T>	&v
	)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

template<class T> 
inline vec3<T> vec3<T>::operator-(
		const T			v
	) const
{
	return vec3<T>(x - v, y - v, z - v);
}

template<class T> 
inline vec3<T> &vec3<T>::operator-=(
		const T			v
	)
{
	x -= v;
	y -= v;
	z -= v;
	return *this;
}

template<class T> 
inline vec3<T> vec3<T>::operator-() const
{
	return vec3<T>(-x, -y, -z);
}


/* Scalar/element-wise multiplication */
template<class T> 
inline vec3<T> vec3<T>::operator*(
		const vec3<T>	&v
	) const
{
	return vec3<T>(x * v.x, y * v.y, z * v.z);
}

template<class T> 
inline vec3<T> &vec3<T>::operator*=(
		const vec3<T>	&v
	)
{
	x *= v.x;
	y *= v.y;
	z *= v.z;
	return *this;
}

template<class T> 
inline vec3<T> vec3<T>::operator*(
		const mat3<T>	&v
	) const
{
	return vec3f(v * (*this));
}

template<class T> 
inline vec3<T> &vec3<T>::operator*=(
		const mat3<T>	&v
	)
{
	vec3f nv = vec3f(v * (*this));
	x = nv.x;
	y = nv.y;
	z = nv.z;
	return *this;
}

template<class T> 
inline vec3<T> vec3<T>::operator*(
		const T			v
	) const
{
	return vec3<T>(x * v, y * v, z * v);
}

template<class T>
inline vec3<T> &vec3<T>::operator*=(
		const T			v
	)
{
	x *= v;
	y *= v;
	z *= v;
	return *this;
}


/* Dot product */
template<class T>
inline T vec3<T>::dot(
		const vec3<T>	&v
	) const
{
	return x * v.x + y * v.y + z * v.z;
}

template<class T>
inline T vec3<T>::operator^(
		const vec3<T>	&v
	) const
{
	return dot(v);
}


/* Cross product */
template<class T>
inline vec3<T> vec3<T>::cross(
		const vec3<T>	&v
	) const
{
	return vec3f(y * v.z - z * v.y,
				 z * v.x - x * v.z,
				 x * v.y - y * v.x);
}

template<class T>
inline vec3<T> vec3<T>::operator%(
		const vec3<T>	&v
	) const
{
	return cross(v);
}


/* Division */
template<class T>
inline vec3<T> vec3<T>::operator/(
		const vec3<T>	&v
	) const
{
	return vec3<T>(x / v.x, y / v.y, z / v.z);
}

template<class T>
inline vec3<T> &vec3<T>::operator/=(
		const vec3<T>	&v
	)
{
	x /= v.x;
	y /= v.y;
	z /= v.z;
	return *this;
}

template<class T>
inline vec3<T> vec3<T>::operator/(
		const T			v
	) const
{
	T t = ((T) 1) / v;
	return vec3<T>(x * t, y * t, z * t);
}

template<class T>
inline vec3<T> &vec3<T>::operator/=(
		const T			v
	)
{
	T t = ((T) 1) / v;
	x *= t;
	y *= t;
	z *= t;
	return *this;
}


/* Misc useful stuff */
template<class T>
inline T vec3<T>::minElement() const
{
	return x < y ? (x < z ? x : z) : (y < z ? y : z);
}

template<class T>
inline T vec3<T>::maxElement() const
{
	return x > y ? (x > z ? x : z) : (y > z ? y : z);
}

template<class T>
inline void vec3<T>::clamp(
		const T			min,
		const T			max
	)
{
	x = sclamp(x, min, max);
	y = sclamp(y, min, max);
	z = sclamp(z, min, max);
}

template<class T>
inline void vec3<T>::clamp(
		const vec3<T>	&min,
		const vec3<T>	&max
	)
{
	x = sclamp(x, min.x, max.x);
	y = sclamp(y, min.y, max.y);
	z = sclamp(z, min.z, max.z);
}

template<class T>
inline void vec3<T>::saturate()
{
	x = ssaturate(x);
	y = ssaturate(y);
	z = ssaturate(z);
}

template<class T>
inline vec3<T> vec3<T>::getSaturated() const
{
	vec3<T> r;
	r.x = ssaturate(x);
	r.y = ssaturate(y);
	r.z = ssaturate(z);
	return r;
}

template<class T>
inline void vec3<T>::normalize()
{
	T t = ((T) 1) / length();
	x *= t;
	y *= t;
	z *= t;
}

template<class T>
inline vec3<T> vec3<T>::getNormalized() const
{
	vec3<T> r;
	T t = ((T) 1) / length();
	
	r.x = x * t;
	r.y = y * t;
	r.z = z * t;
	
	return r;
}

template<class T>
inline T vec3<T>::len() const
{
	return sqrt(x * x + y * y + z * z);
}

template<class T>
inline T vec3<T>::length() const
{
	return len();
}

template<class T>
inline T vec3<T>::len2() const
{
	return x * x + y * y + z * z;
}

template<class T>
inline T vec3<T>::length2() const
{
	return len2();
}

template<class T>
inline T vec3<T>::lengthSquared() const
{
	return len2();
}

template<class T>
inline vec3<T> vec3<T>::reflection(
		const vec3<T>	&v
	) const
{
	T nv2 = dot(v) * ((T) 2);
	return vec3<T>(v.x * nv2 - x, v.y * nv2 - y, v.z * nv2 - z);
}

template<class T>
inline vec3<T> vec3<T>::refraction(
		const vec3<T>	&v,
		const T			r
	) const
{
	T w, k, wk;
	w = -dot(v) * r;
	k = sqrt(((T) 1) + (w - r) * (w + r));
	wk = w - k;
	return vec3<T>(r * x + wk * v.x,
				   r * y + wk * v.y,
				   r * z + wk * v.z);
}

template<class T>
f32 vec3<T>::projectionLengthOn(
		const vec3<T>	&v
	) const
{
	return dot(v) / v.len();
}

template<class T>
vec3<T> vec3<T>::projectOn(
		const vec3<T>	&v
	) const
{
	return v * (dot(v) / (v.dot(v)));
}

template<class T>
inline vec2<T> vec3<T>::xy() const
{
	return vec2<T>(x, y);
}

template<class T>
inline vec4<T> vec3<T>::xyz1() const
{
	return vec4<T>(x, y, z, 1);
}

template<class T>
inline vec4<T> vec3<T>::xyz0() const
{
	return vec4<T>(x, y, z, 0);
}











/* Functions */

/* Assignment */
template<class T>
inline vec4<T> &vec4<T>::operator=(
		const vec4<T>	&v
	)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;

	return *this;
}

template<class T>
inline vec4<T> &vec4<T>::operator=(
		const T			v
	)
{
	x = v;
	y = v;
	z = v;
	w = v;

	return *this;
}


/* Comparison */
template<class T>
inline bool vec4<T>::operator==(
		const vec4<T>	&v
	) const
{
	return x == v.x &&
		y == v.y &&
		z == v.z &&
		w == v.w;
}

template<class T>
inline bool vec4<T>::operator!=(
		const vec4<T>	&v
	) const
{
	return x != v.x ||
		y != v.y ||
		z != v.z ||
		w != v.w;
}


/* Addition */
template<class T> 
inline vec4<T> vec4<T>::operator+(
		const vec4<T>	&v
	) const
{
	return vec4<T>(x + v.x, y + v.y, z + v.z, w + v.w);
}

template<class T> 
inline vec4<T> &vec4<T>::operator+=(
		const vec4<T>	&v
	)
{
	x += v.x;
	y += v.y;
	z += v.z;
	w += v.w;
	return *this;
}

template<class T> 
inline vec4<T> vec4<T>::operator+(
		const T			v
	) const
{
	return vec4<T>(x + v, y + v, z + v, w + v);
}

template<class T> 
inline vec4<T> &vec4<T>::operator+=(
		const T			v
	)
{
	x += v;
	y += v;
	z += v;
	w += v;
	return *this;
}

template<class T> 
inline vec4<T> vec4<T>::operator+() const
{
	return vec4<T>(x, y, z, w);
}


/* Subtract */
template<class T> 
inline vec4<T> vec4<T>::operator-(
		const vec4<T>	&v
	) const
{
	return vec4<T>(x - v.x, y - v.y, z - v.z, w - v.w);
}

template<class T> 
inline vec4<T> &vec4<T>::operator-=(
		const vec4<T>	&v
	)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	w -= v.w;
	return this;
}

template<class T> 
inline vec4<T> vec4<T>::operator-(
		const T			v
	) const
{
	return vec4<T>(x - v, y - v, z - v, w - v);
}

template<class T> 
inline vec4<T> &vec4<T>::operator-=(
		const T			v
	)
{
	x -= v;
	y -= v;
	z -= v;
	w -= v;
	return *this;
}

template<class T> 
inline vec4<T> vec4<T>::operator-() const
{
	return vec4<T>(-x, -y, -z, -w);
}


/* Scalar/element-wise multiplication */
template<class T> 
inline vec4<T> vec4<T>::operator*(
		const vec4<T>	&v
	) const
{
	return vec4<T>(x * v.x, y * v.y, z * v.z, w * v.w);
}

template<class T> 
inline vec4<T> &vec4<T>::operator*=(
		const vec4<T>	&v
	)
{
	x *= v.x;
	y *= v.y;
	z *= v.z;
	w *= v.w;
	return *this;
}

template<class T> 
inline vec4<T> vec4<T>::operator*(
		const mat4<T>	&v
	) const
{
	return vec4f(v * (*this));
}

template<class T> 
inline vec4<T> &vec4<T>::operator*=(
		const mat4<T>	&v
	)
{
	vec4f nv = vec4f(v * (*this));
	x = nv.x;
	y = nv.y;
	z = nv.z;
	w = nv.w;
	return *this;
}

template<class T> 
inline vec4<T> vec4<T>::operator*(
		const T			v
	) const
{
	return vec4<T>(x * v, y * v, z * v, w * v);
}

template<class T>
inline vec4<T> &vec4<T>::operator*=(
		const T			v
	)
{
	x *= v;
	y *= v;
	z *= v;
	w *= v;
	return *this;
}


/* Dot product */
template<class T>
inline T vec4<T>::dot(
		const vec4<T>	&v
	) const
{
	return x * v.x + y * v.y + z * v.z + w * v.w;
}

template<class T>
inline T vec4<T>::operator^(
		const vec4<T>	&v
	) const
{
	return dot(v);
}


/* Division */
template<class T>
inline vec4<T> vec4<T>::operator/(
		const vec4<T>	&v
	) const
{
	return vec4<T>(x / v.x, y / v.y, z / v.z, w / v.w);
}

template<class T>
inline vec4<T> &vec4<T>::operator/=(
		const vec4<T>	&v
	)
{
	x /= v.x;
	y /= v.y;
	z /= v.z;
	w /= v.w;
	return *this;
}

template<class T>
inline vec4<T> vec4<T>::operator/(
		const T			v
	) const
{
	T t = ((T) 1) / v;
	return vec4<T>(x * t, y * t, z * t, w * t);
}

template<class T>
inline vec4<T> &vec4<T>::operator/=(
		const T			v
	)
{
	T t = ((T) 1) / v;
	x *= t;
	y *= t;
	z *= t;
	w *= t;
	return *this;
}


/* Misc useful stuff */
template<class T>
inline T vec4<T>::minElement() const
{
	T a = x < y ? x : y;
	T b = z < w ? z : w;
	return a < b ? a : b;
}

template<class T>
inline T vec4<T>::maxElement() const
{
	T a = x > y ? x : y;
	T b = z > w ? z : w;
	return a > b ? a : b;
}

template<class T>
inline void vec4<T>::clamp(
		const T			min,
		const T			max
	)
{
	x = sclamp(x, min, max);
	y = sclamp(y, min, max);
	z = sclamp(z, min, max);
	w = sclamp(w, min, max);
}

template<class T>
inline void vec4<T>::clamp(
		const vec4<T>	&min,
		const vec4<T>	&max
	)
{
	x = sclamp(x, min.x, max.x);
	y = sclamp(y, min.y, max.y);
	z = sclamp(z, min.z, max.z);
	w = sclamp(w, min.w, max.w);
}

template<class T>
inline void vec4<T>::saturate()
{
	x = ssaturate(x);
	y = ssaturate(y);
	z = ssaturate(z);
	w = ssaturate(w);
}

template<class T>
inline vec4<T> vec4<T>::getSaturated() const
{
	vec4<T> r;
	r.x = ssaturate(x);
	r.y = ssaturate(y);
	r.z = ssaturate(z);
	r.w = ssaturate(w);
	return r;
}

template<class T>
inline void vec4<T>::normalize()
{
	T f = ((T) 1) / length();
	return vec4<T>(x * f, y * f, z * f, w * f);
}

template<class T>
inline vec4<T> vec4<T>::getNormalized() const
{
	vec4<T> r;
	T t = ((T) 1) / length();
	
	r.x = x * t;
	r.y = y * t;
	r.z = z * t;
	r.w = w * t;
	
	return r;
}

template<class T>
inline T vec4<T>::len() const
{
	return sqrt(x * x + y * y + z * z + w * w);
}

template<class T>
inline T vec4<T>::length() const
{
	return len();
}

template<class T>
inline T vec4<T>::len2() const
{
	return x * x + y * y + z * z + w * w;
}

template<class T>
inline T vec4<T>::length2() const
{
	return len2();
}

template<class T>
inline T vec4<T>::lengthSquared() const
{
	return len2();
}


template<class T>
inline vec4<T> vec4<T>::reflection(
		const vec4<T>	&v
	) const
{
	T nv2 = dot(v) * ((T) 2);
	return vec4<T>(x + v.x, y + v.y, z + v.z, w + v.w) * nv2;
}

template<class T>
inline vec4<T> vec4<T>::refraction(
		const vec4<T>	&v,
		const T			r
	) const
{
	T w, k, wk;
	w = -dot(v) * r;
	k = sqrt(((T) 1) + (w - r) * (w + r));
	wk = w - k;
	return vec4<T>(r * x + wk * v.x,
				   r * y + wk * v.y,
				   r * z + wk * v.z,
				   r * w + wk * v.w);
}

template<class T>
f32 vec4<T>::projectionLengthOn(
		const vec4<T>	&v
	) const
{
	return dot(v) / v.len();
}

template<class T>
vec4<T> vec4<T>::projectOn(
		const vec4<T>	&v
	) const
{
	return v * (dot(v) / (v.dot(v)));
}

template<class T>
inline vec3<T> vec4<T>::xyz() const
{
	return vec3<T>(x, y, z);
}

template<class T>
inline vec2<T> vec4<T>::xy() const
{
	return vec2<T>(x, y);
}









/* Functions */

/* Assignment */
template<class T>
inline mat2<T> &mat2<T>::operator=(
		const mat2<T>	&v
	)
{
	a = v.a;
	b = v.b;
	c = v.c;
	d = v.d;

	return *this;
}


/* Comparison */
template<class T>
inline bool mat2<T>::operator==(
		const mat2<T>	&v
	) const
{
	return a == v.a &&
		b == v.b &&
		c == v.c &&
		d == v.d;
}

template<class T>
inline bool mat2<T>::operator!=(
		const mat2<T>	&v
	) const
{
	return a != v.a ||
		b != v.b ||
		c != v.c ||
		d != v.d;
}

template<class T>
inline bool mat2<T>::isIdentity() const
{
	return a == 1 &&
		b == 0 &&
		c == 0 &&
		d == 1;
}


/* Addition */
template<class T>
inline mat2<T> mat2<T>::operator+(
		const mat2<T>	&v
	) const
{
	return mat2<T>(
		a + v.a,
		b + v.b,
		c + v.c,
		d + v.d);
}

template<class T>
inline mat2<T> &mat2<T>::operator+=(
		const mat2<T>	&v
	)
{
	a += v.a;
	b += v.b;
	c += v.c;
	d += v.d;
	return *this;
}

template<class T>
inline mat2<T> mat2<T>::operator+(
		const T			v
	) const
{
	return mat2<T>(
		a + v,
		b + v,
		c + v,
		d + v);
}

template<class T>
inline mat2<T> &mat2<T>::operator+=(
		const T			v
	)
{
	a += v;
	b += v;
	c += v;
	d += v;
	return *this;
}

template<class T>
inline mat2<T> mat2<T>::operator+() const
{
	return mat2<T>(
		a,
		b,
		c,
		d);
}


/* Subtraction */
template<class T>
inline mat2<T> mat2<T>::operator-(
		const mat2<T>	&v
	) const
{
	return mat2<T>(
		a - v.a,
		b - v.b,
		c - v.c,
		d - v.d);
}

template<class T>
inline mat2<T> &mat2<T>::operator-=(
		const mat2<T>	&v
	)
{
	a -= v.a;
	b -= v.b;
	c -= v.c;
	d -= v.d;
	return *this;
}

template<class T>
inline mat2<T> mat2<T>::operator-(
		const T			v
	) const
{
	return mat2<T>(
		a - v,
		b - v,
		c - v,
		d - v);
}

template<class T>
inline mat2<T> &mat2<T>::operator-=(
		const T			v
	)
{
	a -= v;
	b -= v;
	c -= v;
	d -= v;
	return this;
}

template<class T>
inline mat2<T> mat2<T>::operator-() const
{
	return mat2<T>(
		-a,
		-b,
		-c,
		-d);
}


/* Inner product */
template<class T>
inline T mat2<T>::iprod(
		const mat2<T>	&v,
		const u32		row,
		const u32		col
	) const
{
//	return t[0][row] * v.t[col][0] +
//		t[1][row] * v.t[col][1];

	return mat[row][0] * v.mat[0][col] +
		mat[row][1] * v.mat[1][col];
}


/* Multiplication */
template<class T>
inline mat2<T> mat2<T>::operator*(
		const mat2<T>	&v
	) const
{
	return mat2<T>(
			iprod(v, 0, 0),
			iprod(v, 0, 1),
			iprod(v, 1, 0),
			iprod(v, 1, 1));
}

template<class T>
inline mat2<T> &mat2<T>::operator*=(
		const mat2<T>	&v
	)
{
	mat2<T> mat = mat2<T>(
			iprod(v, 0, 0),
			iprod(v, 0, 1),

			iprod(v, 1, 0),
			iprod(v, 1, 1));

	a = mat.a;
	b = mat.b;
	c = mat.c;
	d = mat.d;
	return *this;
}

template<class T>
inline mat2<T> mat2<T>::operator*(
		const T			v
	) const
{
	return mat2<T>(
		a * v,
		b * v,
		c * v,
		d * v);
}

template<class T>
inline mat2<T> &mat2<T>::operator*=(
		const T			v
	)
{
	a *= v;
	b *= v;
	c *= v;
	d *= v;
	return *this;
}

template<class T>
inline vec2<T> mat2<T>::operator*(
		const vec2<T>	&v
	) const
{
	return vec2<T>(
		a * v.x + b * v.y,
		c * v.x + d * v.y
	);
}


/* Element-wise multiplication */
template<class T>
inline mat2<T> mat2<T>::elemMul(
		const mat2<T>	&v
	) const
{
	return mat2<T>(
		a * v.a,
		b * v.b,
		c * v.c,
		d * v.d);
}


/* Division with scalar*/
template<class T>
inline mat2<T> mat2<T>::operator/(
		const T			v
	) const
{
	T f = ((T) 1) / v;
	return mat2<T>(
		a * f,
		b * f,
		c * f,
		d * f);
}

template<class T>
inline mat2<T> &mat2<T>::operator/=(
		const T			v
	)
{
	T f = ((T) 1) / v;
	a *= f;
	b *= f;
	c *= f;
	d *= f;
	return *this;
}


/* Misc matrix operations */
template<class T>
inline void mat2<T>::identity()
{
	a = 1;
	b = 0;
	c = 0;
	d = 1;
}

template<class T>
inline void mat2<T>::transpose()
{
	sswap(b, c);
}

template<class T>
inline T mat2<T>::det() const
{
	return a * d - b * c;
}

template<class T>
inline T mat2<T>::determinant() const
{
	return det();
}

template<class T>
inline void mat2<T>::inv()
{
	mat2<T> mat = *this;
	T deter = det();
	T denom;

	if (deter == 0) {
		a = (T) 1;
		b = (T) 0;
		c = (T) 0;
		d = (T) 1;
		return;
	}

	denom = ((T) 1) / deter();

	a = mat.d * denom;
	b = -mat.b * denom;
	c = -mat.c * denom;
	d = mat.a * denom;
}

template<class T>
inline void mat2<T>::inverse()
{
	inv();
}

template<class T>
inline T mat2<T>::trace() const
{
	return a + d;
}

template<class T>
inline vec2<T> mat2<T>::eigenvalues() const
{
	vec2<T> ret;

	T t = trace();
	T u = sqrt(t * t - ((T) 4) * det());

	ret.x = t - u;
	ret.y = t + u;

	return ret;
}

template<class T>
inline mat2<T> mat2<T>::eigenvectors(
		const vec2<T>	&eig
	) const
{
	if (b != 0) {
		return mat2<T>(b, b, eig.x - a, eig.y - a);
	}
	
	if (c != 0) {
		return mat2<T>(eig.x - d, eig.y - d, c, c);
	}

	return mat2<T>(1, 0, 0, 1);

}

template<class T>
inline vec2<T> mat2<T>::getRow(
		u32		row
	) const
{
	return vec2<T>(
		mat[row][0],
		mat[row][1]);
}

template<class T>
inline void mat2<T>::setRow(
		u32		row,
		vec2<T>	vec
	)
{
	mat[row][0] = vec.x;
	mat[row][1] = vec.y;
}

template<class T>
inline vec2<T> mat2<T>::getCol(
		u32		col
	) const
{
	return vec2<T>(
		mat[0][col],
		mat[1][col]);
}

template<class T>
inline void mat2<T>::setCol(
		u32		col,
		vec2<T>	vec
	)
{
	mat[0][col] = vec.x;
	mat[1][col] = vec.y;
}

template<class T>
vec2<T> mat2<T>::solveSystem(
		vec2<T>	right_side
	) const
{
	T invd = ((T) 1) / (a * d - b * c);
	T sx = (right_side.x * d - b * right_side.y) * invd;
	T sy = (a * right_side.x - right_side.y * c) * invd;
	return vec2<T>(sx, sy);
}








/* Functions */

/* Assignment */
template<class T>
inline mat3<T> &mat3<T>::operator=(
		const mat3<T>	&v
	)
{
	a = v.a;
	b = v.b;
	c = v.c;

	d = v.d;
	e = v.e;
	f = v.f;

	g = v.g;
	h = v.h;
	i = v.i;

	return *this;
}


/* Comparison */
template<class T>
inline bool mat3<T>::operator==(
		const mat3<T>	&v
	) const
{
	return a == v.a &&
		b == v.b &&
		c == v.c &&

		d == v.d &&
		e == v.e &&
		f == v.f &&

		g == v.g &&
		h == v.h &&
		i == v.i;
}

template<class T>
inline bool mat3<T>::operator!=(
		const mat3<T>	&v
	) const
{
	return a != v.a ||
		b != v.b ||
		c != v.c ||

		d != v.d ||
		e != v.e ||
		f != v.f ||

		g != v.g ||
		h != v.h ||
		i != v.i;
}

template<class T>
inline bool mat3<T>::isIdentity() const
{
	return a == 1 &&
		b == 0 &&
		c == 0 &&

		d == 0 &&
		e == 1 &&
		f == 0 &&

		g == 0 &&
		h == 0 &&
		i == 1;
}


/* Addition */
template<class T>
inline mat3<T> mat3<T>::operator+(
		const mat3<T>	&v
	) const
{
	return mat3<T>(
		a + v.a,
		b + v.b,
		c + v.c,

		d + v.d,
		e + v.e,
		f + v.f,

		g + v.g,
		h + v.h,
		i + v.i);
}

template<class T>
inline mat3<T> &mat3<T>::operator+=(
		const mat3<T>	&v
	)
{
	a += v.a;
	b += v.b;
	c += v.c;

	d += v.d;
	e += v.e;
	f += v.f;

	g += v.g;
	h += v.h;
	i += v.i;

	return *this;
}

template<class T>
inline mat3<T> mat3<T>::operator+(
		const T			v
	) const
{
	return mat3<T>(
		a + v,
		b + v,
		c + v,

		d + v,
		e + v,
		f + v,

		g + v,
		h + v,
		i + v);
}

template<class T>
inline mat3<T> &mat3<T>::operator+=(
		const T			v
	)
{
	a += v;
	b += v;
	c += v;

	d += v;
	e += v;
	f += v;

	g += v;
	h += v;
	i += v;

	return *this;
}

template<class T>
inline mat3<T> mat3<T>::operator+() const
{
	return mat3<T>(
		a, b, c,
		d, e, f,
		g, h, i);
}


/* Subtraction */
template<class T>
inline mat3<T> mat3<T>::operator-(
		const mat3<T>	&v
	) const
{
	return mat3<T>(
		a - v.a,
		b - v.b,
		c - v.c,

		d - v.d,
		e - v.e,
		f - v.f,

		g - v.g,
		h - v.h,
		i - v.i);
}

template<class T>
inline mat3<T> &mat3<T>::operator-=(
		const mat3<T>	&v
	)
{
	a -= v.a;
	b -= v.b;
	c -= v.c;

	d -= v.d;
	e -= v.e;
	f -= v.f;

	g -= v.g;
	h -= v.h;
	i -= v.i;

	return *this;
}

template<class T>
inline mat3<T> mat3<T>::operator-(
		const T			v
	) const
{
	return mat3<T>(
		a - v,
		b - v,
		c - v,

		d - v,
		e - v,
		f - v,

		g - v,
		h - v,
		i - v);
}

template<class T>
inline mat3<T> &mat3<T>::operator-=(
		const T			v
	)
{
	a -= v;
	b -= v;
	c -= v;

	d -= v;
	e -= v;
	f -= v;

	g -= v;
	h -= v;
	i -= v;

	return *this;
}

template<class T>
inline mat3<T> mat3<T>::operator-() const
{
	return mat3<T>(
		-a, -b, -c,
		-d, -e, -f,
		-g, -h, -i);
}


/* Inner product */
template<class T>
inline T mat3<T>::iprod(
		const mat3<T>	&v,
		const u32		row,
		const u32		col
	) const
{
//	return t[0][row] * v.t[col][0] +
//		t[1][row] * v.t[col][1] +
//		t[2][row] * v.t[col][2];
	return mat[row][0] * v.mat[0][col] +
		mat[row][1] * v.mat[1][col] +
		mat[row][2] * v.mat[2][col];
}


/* Multiplication */
template<class T>
inline mat3<T> mat3<T>::operator*(
		const mat3<T>	&v
	) const
{
	return mat3<T>(
			iprod(v, 0, 0),
			iprod(v, 0, 1),
			iprod(v, 0, 2),

			iprod(v, 1, 0),
			iprod(v, 1, 1),
			iprod(v, 1, 2),

			iprod(v, 2, 0),
			iprod(v, 2, 1),
			iprod(v, 2, 2));
}

template<class T>
inline mat3<T> &mat3<T>::operator*=(
		const mat3<T>	&v
	)
{
	mat3<T> mat = mat3<T>(
			iprod(v, 0, 0),
			iprod(v, 0, 1),
			iprod(v, 0, 2),

			iprod(v, 1, 0),
			iprod(v, 1, 1),
			iprod(v, 1, 2),

			iprod(v, 2, 0),
			iprod(v, 2, 1),
			iprod(v, 2, 2));

	a = mat.a;
	b = mat.b;
	c = mat.c;

	d = mat.d;
	e = mat.e;
	f = mat.f;

	g = mat.g;
	h = mat.h;
	i = mat.i;

	return *this;
}

template<class T>
inline mat3<T> mat3<T>::operator*(
		const T			v
	) const
{
	return mat3<T>(
		a * v,
		b * v,
		c * v,

		d * v,
		e * v,
		f * v,

		g * v,
		h * v,
		i * v);
}

template<class T>
inline mat3<T> &mat3<T>::operator*=(
		const T			v
	)
{
	a *= v;
	b *= v;
	c *= v;

	d *= v;
	e *= v;
	f *= v;

	g *= v;
	h *= v;
	i *= v;

	return *this;
}

template<class T>
inline vec3<T> mat3<T>::operator*(
		const vec3<T>	&v
	) const
{
	return vec3<T>(
		a * v.x + b * v.y + c * v.z,
		d * v.x + e * v.y + f * v.z,
		g * v.x + h * v.y + i * v.z
	);
}


/* Element-wise multiplication */
template<class T>
inline mat3<T> mat3<T>::elemMul(
		const mat3<T>	&v
	) const
{
	return mat3<T>(
		a * v.a,
		b * v.b,
		c * v.c,

		d * v.d,
		e * v.e,
		f * v.f,

		g * v.g,
		h * v.h,
		i * v.i);
}



/* Division with scalar*/
template<class T>
inline mat3<T> mat3<T>::operator/(
		const T			v
	) const
{
	T f = ((T) 1) / v;
	return mat3<T>(
		a * f,
		b * f,
		c * f,

		d * f,
		e * f,
		f * f,

		g * f,
		h * f,
		i * f);
}

template<class T>
inline mat3<T> &mat3<T>::operator/=(
		const T			v
	)
{
	T f = ((T) 1) / v;
	a *= f;
	b *= f;
	c *= f;

	d *= f;
	e *= f;
	f *= f;

	g *= f;
	h *= f;
	i *= f;

	return *this;
}


/* Misc matrix operations */
template<class T>
inline void mat3<T>::identity()
{
	a = e = i = 1;
	b = c = 0;
	d = f = 0;
	g = h = 0;
}

template<class T>
inline void mat3<T>::transpose()
{
	sswap(b, d);
	sswap(c, g);
	sswap(f, h);
}

template<class T>
inline T mat3<T>::det() const
{
	return a * (i * e - h * f) -
		d * (i * b - h * c) +
		g * (f * b - e * c);
}

template<class T>
inline T mat3<T>::determinant() const
{
	return det();
}

template<class T>
inline void mat3<T>::inv()
{
	T ie = i * e;
	T ib = i * b;
	T fb = f * b;
	T hf = h * f;
	T hc = h * c;
	T ec = e * c;

	T deter = a * (ie - hf) - d * (ib - hc) + g * (fb - ec);

	if (deter == ((T) 0)) {
		identity();
		return;
	}

	mat3<T> mat = *this;
	T denom = ((T) 1) / deter;

	a = (ie            - hf           ) * denom;
	b = (hc            - ib           ) * denom;
	c = (fb            - ec           ) * denom;

	d = (mat.f * mat.g - mat.d * mat.i) * denom;
	e = (mat.a * mat.i - mat.c * mat.g) * denom;
	f = (mat.d * mat.c - mat.a * mat.f) * denom;

	g = (mat.d * mat.h - mat.e * mat.g) * denom;
	h = (mat.b * mat.g - mat.a * mat.h) * denom;
	i = (mat.a * mat.e - mat.b * mat.d) * denom;
}

template<class T>
inline void mat3<T>::inverse()
{
	inv();
}

template<class T>
inline T mat3<T>::trace() const
{
	return a + e + i;
}

template<class T>
inline vec3<T> mat3<T>::getRow(
		u32		row
	) const
{
	return vec4<T>(
		mat[row][0],
		mat[row][1],
		mat[row][2]);
}

template<class T>
inline void mat3<T>::setRow(
		u32		row,
		vec3<T>	vec
	)
{
	mat[row][0] = vec.x;
	mat[row][1] = vec.y;
	mat[row][2] = vec.z;
}

template<class T>
inline vec3<T> mat3<T>::getCol(
		u32		col
	) const
{
	return vec3<T>(
		mat[0][col],
		mat[1][col],
		mat[2][col]);
}

template<class T>
inline void mat3<T>::setCol(
		u32		col,
		vec3<T>	vec
	)
{
	mat[0][col] = vec.x;
	mat[1][col] = vec.y;
	mat[2][col] = vec.z;
}

template<class T>
vec3<T> mat3<T>::up() const
{
	return vec3f(b, e, h);
}

template<class T>
vec3<T> mat3<T>::down() const
{
	return -up();
}

template<class T>
vec3<T> mat3<T>::left() const
{
	return -right();
}

template<class T>
vec3<T> mat3<T>::right() const
{
	return vec3f(a, d, g);
}

template<class T>
vec3<T> mat3<T>::front() const
{
	return -back();
}

template<class T>
vec3<T> mat3<T>::back() const
{
	return vec3f(c, f, i);
}
















/* Functions */

/* Assignment */
template<class T>
inline mat4<T> &mat4<T>::operator=(
		const mat4<T>	&v
	)
{
	a = v.a;
	b = v.b;
	c = v.c;
	d = v.d;

	e = v.e;
	f = v.f;
	g = v.g;
	h = v.h;

	i = v.i;
	j = v.j;
	k = v.k;
	l = v.l;

	m = v.m;
	n = v.n;
	o = v.o;
	p = v.p;

	return *this;
}


/* Comparison */
template<class T>
inline bool mat4<T>::operator==(
		const mat4<T>	&v
	) const
{
	return a == v.a &&
		b == v.b &&
		c == v.c &&
		d == v.d &&

		e == v.e &&
		f == v.f &&
		g == v.g &&
		h == v.h &&

		i == v.i &&
		j == v.j &&
		k == v.k &&
		l == v.l &&

		m == v.m &&
		n == v.n &&
		o == v.o &&
		p == v.p;
}

template<class T>
inline bool mat4<T>::operator!=(
		const mat4<T>	&v
	) const
{
	return a != v.a ||
		b != v.b ||
		c != v.c ||
		d != v.d ||

		e != v.e ||
		f != v.f ||
		g != v.g ||
		h != v.h ||

		i != v.i ||
		j != v.j ||
		k != v.k ||
		l != v.l ||

		m != v.m ||
		n != v.n ||
		o != v.o ||
		p != v.p;
}

template<class T>
inline bool mat4<T>::isIdentity() const
{
	return a == 1 &&
		b == 0 &&
		c == 0 &&
		d == 0 &&

		e == 0 &&
		f == 1 &&
		g == 0 &&
		h == 0 &&

		i == 0 &&
		j == 0 &&
		k == 1 &&
		l == 0 &&

		m == 0 &&
		n == 0 &&
		o == 0 &&
		p == 1;
}


/* Addition */
template<class T>
inline mat4<T> mat4<T>::operator+(
		const mat4<T>	&v
	) const
{
	return mat4<T>(
		a + v.a,
		b + v.b,
		c + v.c,
		d + v.d,

		e + v.e,
		f + v.f,
		g + v.g,
		h + v.h,

		i + v.i,
		j + v.j,
		k + v.k,
		l + v.l,

		m + v.m,
		n + v.n,
		o + v.o,
		p + v.p);
}

template<class T>
inline mat4<T> &mat4<T>::operator+=(
		const mat4<T>	&v
	)
{
	a += v.a;
	b += v.b;
	c += v.c;
	d += v.d;

	e += v.e;
	f += v.f;
	g += v.g;
	h += v.h;

	i += v.i;
	j += v.j;
	k += v.k;
	l += v.l;

	m += v.m;
	n += v.n;
	o += v.o;
	p += v.p;

	return *this;
}

template<class T>
inline mat4<T> mat4<T>::operator+(
		const T			v
	) const
{
	return mat4<T>(
		a + v,
		b + v,
		c + v,
		d + v,

		e + v,
		f + v,
		g + v,
		h + v,

		i + v,
		j + v,
		k + v,
		l + v,

		m + v,
		n + v,
		o + v,
		p + v);
}

template<class T>
inline mat4<T> &mat4<T>::operator+=(
		const T			v
	)
{
	a += v;
	b += v;
	c += v;
	d += v;

	e += v;
	f += v;
	g += v;
	h += v;

	i += v;
	j += v;
	k += v;
	l += v;

	m += v;
	n += v;
	o += v;
	p += v;

	return *this;
}

template<class T>
inline mat4<T> mat4<T>::operator+() const
{
	return mat4<T>(
		a, b, c, d,
		e, f, g, h,
		i, j, k, l,
		m, n, o, p);
}


/* Subtraction */
template<class T>
inline mat4<T> mat4<T>::operator-(
		const mat4<T>	&v
	) const
{
	return mat4<T>(
		a - v.a,
		b - v.b,
		c - v.c,
		d - v.d,

		e - v.e,
		f - v.f,
		g - v.g,
		h - v.h,

		i - v.i,
		j - v.j,
		k - v.k,
		l - v.l,

		m - v.m,
		n - v.n,
		o - v.o,
		p - v.p);
}

template<class T>
inline mat4<T> &mat4<T>::operator-=(
		const mat4<T>	&v
	)
{
	a -= v.a;
	b -= v.b;
	c -= v.c;
	d -= v.d;

	e -= v.e;
	f -= v.f;
	g -= v.g;
	h -= v.h;

	i -= v.i;
	j -= v.j;
	k -= v.k;
	l -= v.l;

	m -= v.m;
	n -= v.n;
	o -= v.o;
	p -= v.p;

	return *this;
}

template<class T>
inline mat4<T> mat4<T>::operator-(
		const T			v
	) const
{
	return mat4<T>(
		a - v,
		b - v,
		c - v,
		d - v,

		e - v,
		f - v,
		g - v,
		h - v,

		i - v,
		j - v,
		k - v,
		l - v,

		m - v,
		n - v,
		o - v,
		p - v);
}

template<class T>
inline mat4<T> &mat4<T>::operator-=(
		const T			v
	)
{
	a -= v;
	b -= v;
	c -= v;
	d -= v;

	e -= v;
	f -= v;
	g -= v;
	h -= v;

	i -= v;
	j -= v;
	k -= v;
	l -= v;

	m -= v;
	n -= v;
	o -= v;
	p -= v;

	return *this;
}

template<class T>
inline mat4<T> mat4<T>::operator-() const
{
	return mat4<T>(
		-a, -b, -c, -d,
		-e, -f, -g, -h,
		-i, -j, -k, -l,
		-m, -n, -o, -p);
}


/* Inner product */
template<class T>
inline T mat4<T>::iprod(
		const mat4<T>	&v,
		const u32		row,
		const u32		col
	) const
{
	/*
	return t[0][row] * v.t[col][0] +
		t[1][row] * v.t[col][1] +
		t[2][row] * v.t[col][2] +
		t[3][row] * v.t[col][3];
		*/
	return mat[row][0] * v.mat[0][col] +
		mat[row][1] * v.mat[1][col] +
		mat[row][2] * v.mat[2][col] +
		mat[row][3] * v.mat[3][col];

}


/* Multiplication */
template<class T>
inline mat4<T> mat4<T>::operator*(
		const mat4<T>	&v
	) const
{
	return mat4<T>(
			iprod(v, 0, 0),
			iprod(v, 0, 1),
			iprod(v, 0, 2),
			iprod(v, 0, 3),

			iprod(v, 1, 0),
			iprod(v, 1, 1),
			iprod(v, 1, 2),
			iprod(v, 1, 3),

			iprod(v, 2, 0),
			iprod(v, 2, 1),
			iprod(v, 2, 2),
			iprod(v, 2, 3),

			iprod(v, 3, 0),
			iprod(v, 3, 1),
			iprod(v, 3, 2),
			iprod(v, 3, 3));

}

template<class T>
inline mat4<T> &mat4<T>::operator*=(
		const mat4<T>	&v
	)
{
	mat4<T> mat = mat4<T>(
			iprod(v, 0, 0),
			iprod(v, 0, 1),
			iprod(v, 0, 2),
			iprod(v, 0, 3),

			iprod(v, 1, 0),
			iprod(v, 1, 1),
			iprod(v, 1, 2),
			iprod(v, 1, 3),

			iprod(v, 2, 0),
			iprod(v, 2, 1),
			iprod(v, 2, 2),
			iprod(v, 2, 3),

			iprod(v, 3, 0),
			iprod(v, 3, 1),
			iprod(v, 3, 2),
			iprod(v, 3, 3));

	a = mat.a;
	b = mat.b;
	c = mat.c;
	d = mat.d;

	e = mat.e;
	f = mat.f;
	g = mat.g;
	h = mat.h;

	i = mat.i;
	j = mat.j;
	k = mat.k;
	l = mat.l;

	m = mat.m;
	n = mat.n;
	o = mat.o;
	p = mat.p;

	return *this;
}



template<class T>
inline T mat4<T>::affineiprod0(
		const mat4<T>	&v,
		const u32		row,
		const u32		col
	) const
{
	return mat[row][0] * v.mat[0][col] +
		mat[row][1] * v.mat[1][col] +
		mat[row][2] * v.mat[2][col];

}

template<class T>
inline T mat4<T>::affineiprod1(
		const mat4<T>	&v,
		const u32		row,
		const u32		col
	) const
{
	return mat[row][0] * v.mat[0][col] +
		mat[row][1] * v.mat[1][col] +
		mat[row][2] * v.mat[2][col] +
		mat[row][3];
}

template<class T>
inline mat4<T> mat4<T>::affineMul(
		const mat4<T>	&v
	) const
{
	return mat4<T>(
			affineiprod0(v, 0, 0),
			affineiprod0(v, 0, 1),
			affineiprod0(v, 0, 2),
			affineiprod1(v, 0, 3),

			affineiprod0(v, 1, 0),
			affineiprod0(v, 1, 1),
			affineiprod0(v, 1, 2),
			affineiprod1(v, 1, 3),

			affineiprod0(v, 2, 0),
			affineiprod0(v, 2, 1),
			affineiprod0(v, 2, 2),
			affineiprod1(v, 2, 3),

			((T) 0),
			((T) 0),
			((T) 0),
			((T) 1));
}

template<class T>
inline vec4<T> mat4<T>::affineMul(
		const vec4<T>	&v
	) const
{
	return vec4<T>(
		a * v.x + b * v.y + c * v.z + d * v.w,
		e * v.x + f * v.y + g * v.z + h * v.w,
		i * v.x + j * v.y + k * v.z + l * v.w,
		v.w
	);
}

template<class T>
inline vec3<T> mat4<T>::affineMul(
		const vec3<T>	&v
	) const
{
	return vec3<T>(
		a * v.x + b * v.y + c * v.z + d,
		e * v.x + f * v.y + g * v.z + h,
		i * v.x + j * v.y + k * v.z + l
	);
}

template<class T>
inline mat4<T> mat4<T>::operator*(
		const T			v
	) const
{
	return mat4<T>(
		a * v,
		b * v,
		c * v,
		d * v,

		e * v,
		f * v,
		g * v,
		h * v,

		i * v,
		j * v,
		k * v,
		l * v,

		m * v,
		n * v,
		o * v,
		p * v);
}

template<class T>
inline mat4<T> &mat4<T>::operator*=(
		const T			v
	)
{
	a *= v;
	b *= v;
	c *= v;
	d *= v;

	e *= v;
	f *= v;
	g *= v;
	h *= v;

	i *= v;
	j *= v;
	k *= v;
	l *= v;

	m *= v;
	n *= v;
	o *= v;
	p *= v;

	return *this;
}

template<class T>
inline vec4<T> mat4<T>::operator*(
		const vec4<T>	&v
	) const
{
	return vec4<T>(
		a * v.x + b * v.y + c * v.z + d * v.w,
		e * v.x + f * v.y + g * v.z + h * v.w,
		i * v.x + j * v.y + k * v.z + l * v.w,
		m * v.x + n * v.y + o * v.z + p * v.w
	);
}


/* Element-wise multiplication */
template<class T>
inline mat4<T> mat4<T>::elemMul(
		const mat4<T>	&v
	) const
{
	return mat4<T>(
		a * v.a,
		b * v.b,
		c * v.c,
		d * v.d,

		e * v.e,
		f * v.f,
		g * v.g,
		h * v.h,

		i * v.i,
		j * v.j,
		k * v.k,
		l * v.l,

		m * v.m,
		n * v.n,
		o * v.o,
		p * v.p);
}


/* Division with scalar*/
template<class T>
inline mat4<T> mat4<T>::operator/(
		const T			v
	) const
{
	T t = ((T) 1) / v;
	return mat4<T>(
		a * t,
		b * t,
		c * t,
		d * t,

		e * t,
		f * t,
		g * t,
		h * t,

		i * t,
		j * t,
		k * t,
		l * t,

		m * t,
		n * t,
		o * t,
		p * t);
}

template<class T>
inline mat4<T> &mat4<T>::operator/=(
		const T			v
	)
{
	T t = ((T) 1) / v;
	a *= t;
	b *= t;
	c *= t;
	d *= t;

	e *= t;
	f *= t;
	g *= t;
	h *= t;

	i *= t;
	j *= t;
	k *= t;
	l *= t;

	m *= t;
	n *= t;
	o *= t;
	p *= t;

	return *this;
}


/* Misc matrix operations */
template<class T>
inline void mat4<T>::identity()
{
	a = f = k = p = 1;
	b = c = d = 0;
	e = g = h = 0;
	i = j = l = 0;
	m = n = o = 0;
}

template<class T>
inline void mat4<T>::transpose()
{
	sswap(e, b);
	sswap(i, c);
	sswap(m, d);
	sswap(j, g);
	sswap(n, h);
	sswap(o, l);
}

template<class T>
inline T mat4<T>::det2(
		T n0,
		T n1,
		T n2,
		T n3
	) const
{
	return n0 * n3 - n1 * n2;
}

template<class T>
inline T mat4<T>::det() const
{
	T s0, s1, s2, s3, s4, s5;
	T c0, c1, c2, c3, c4, c5;

	s0 = det2(a, b, e, f);
	s1 = det2(a, c, e, g);
	s2 = det2(a, d, e, h);
	s3 = det2(b, c, f, g);
	s4 = det2(b, d, f, h);
	s5 = det2(c, d, g, h);

	c5 = det2(k, l, o, p);
	c4 = det2(j, l, n, p);
	c3 = det2(j, k, n, o);
	c2 = det2(i, l, m, p);
	c1 = det2(i, k, m, o);
	c0 = det2(i, j, m, n);

	return s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;
}

template<class T>
inline T mat4<T>::determinant() const
{
	return det();
}

template<class T>
inline void mat4<T>::inv()
{
	T s0, s1, s2, s3, s4, s5;
	T c0, c1, c2, c3, c4, c5;
	T deter, denom;

	s0 = det2(a, b, e, f);
	s1 = det2(a, c, e, g);
	s2 = det2(a, d, e, h);
	s3 = det2(b, c, f, g);
	s4 = det2(b, d, f, h);
	s5 = det2(c, d, g, h);

	c5 = det2(k, l, o, p);
	c4 = det2(j, l, n, p);
	c3 = det2(j, k, n, o);
	c2 = det2(i, l, m, p);
	c1 = det2(i, k, m, o);
	c0 = det2(i, j, m, n);

	deter = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;

	if (deter == 0) {
		identity();
		return;
	}

	denom = ((T) 1) / deter;

	mat4<T> mat = *this;

	a = (mat.f * c5 - mat.g * c4 + mat.h * c3) * denom;
	b = (-mat.b * c5 + mat.c * c4 - mat.d * c3) * denom;
	c = (mat.n * s5 - mat.o * s4 + mat.p * s3) * denom;
	d = (-mat.j * s5 + mat.k * s4 - mat.l * s3) * denom;

	e = (-mat.e * c5 + mat.g * c2 - mat.h * c1) * denom;
	f = (mat.a * c5 - mat.c * c2 + mat.d * c1) * denom;
	g = (-mat.m * s5 + mat.o * s2 - mat.p * s1) * denom;
	h = (mat.i * s5 - mat.k * s2 + mat.l * s1) * denom;

	i = (mat.e * c4 - mat.f * c2 + mat.h * c0) * denom;
	j = (-mat.a * c4 + mat.b * c2 - mat.d * c0) * denom;
	k = (mat.m * s4 - mat.n * s2 + mat.p * s0) * denom;
	l = (-mat.i * s4 + mat.j * s2 - mat.l * s0) * denom;

	m = (-mat.e * c3 + mat.f * c1 - mat.g * c0) * denom;
	n = (mat.a * c3 - mat.b * c1 + mat.c * c0) * denom;
	o = (-mat.m * s3 + mat.n * s1 - mat.o * s0) * denom;
	p = (mat.i * s3 - mat.j * s1 + mat.k * s0) * denom;

}

template<class T>
inline void mat4<T>::inverse()
{
	inv();
}

#if 0
template<class T>
inline void mat4<T>::invertAffine()
{
	/* m, n, o = 0, p = 1 */
	T s0, s1, s2, s3, s4, s5;
	//T c0, c1, c2, c3, c4, c5;
	T deter, denom;

	s0 = det2(a, b, e, f);
	s1 = det2(a, c, e, g);
	s2 = det2(a, d, e, h);
	s3 = det2(b, c, f, g);
	s4 = det2(b, d, f, h);
	s5 = det2(c, d, g, h);

	c5 = k;
	c4 = j;
	c3 = 0;
	c2 = i;
	c1 = 0;
	c0 = 0;

	deter = s0 * k - s1 * j + s3 * i;

	if (deter == 0) {
		identity();
		return;
	}

	denom = ((T) 1) / deter;

	mat4<T> mat = *this;

	a = ( mat.f * k  - mat.g * j			  ) * denom;
	b = (-mat.b * k  + mat.c * j			  ) * denom;
	c = ( mat.n * s5 - mat.o * s4 + mat.p * s3) * denom;
	d = (-mat.j * s5 + mat.k * s4 - mat.l * s3) * denom;

	e = (-mat.e * k  + mat.g * i			  ) * denom;
	f = ( mat.a * k  - mat.c * i			  ) * denom;
	g = (-mat.m * s5 + mat.o * s2 - mat.p * s1) * denom;
	h = ( mat.i * s5 - mat.k * s2 + mat.l * s1) * denom;

	i = ( mat.e * c4 - mat.f * c2 + mat.h * c0) * denom;
	j = (-mat.a * c4 + mat.b * c2 - mat.d * c0) * denom;
	k = ( mat.m * s4 - mat.n * s2 + mat.p * s0) * denom;
	l = (-mat.i * s4 + mat.j * s2 - mat.l * s0) * denom;

	m = (-mat.e * c3 + mat.f * c1 - mat.g * c0) * denom;
	n = (mat.a * c3 - mat.b * c1 + mat.c * c0) * denom;
	o = (-mat.m * s3 + mat.n * s1 - mat.o * s0) * denom;
	p = (mat.i * s3 - mat.j * s1 + mat.k * s0) * denom;

}
#endif

template<class T>
inline T mat4<T>::trace() const
{
	return a + f + k + p;
}

template<class T>
inline mat3<T> mat4<T>::rotationMatrix() const
{
	return mat3<T>(a, b, c,
		e, f, g,
		i, j, k);
}

template<class T>
inline vec4<T> mat4<T>::getRow(
		u32		row
	) const
{
	return vec4<T>(
		mat[row][0],
		mat[row][1],
		mat[row][2],
		mat[row][3]);
}

template<class T>
inline void mat4<T>::setRow(
		u32		row,
		vec4<T>	vec
	)
{
	mat[row][0] = vec.x;
	mat[row][1] = vec.y;
	mat[row][2] = vec.z;
	mat[row][3] = vec.w;
}

template<class T>
inline vec4<T> mat4<T>::getCol(
		u32		col
	) const
{
	return vec4<T>(
		mat[0][col],
		mat[1][col],
		mat[2][col],
		mat[3][col]);
}

template<class T>
inline void mat4<T>::setCol(
		u32		col,
		vec4<T>	vec
	)
{
	mat[0][col] = vec.x;
	mat[1][col] = vec.y;
	mat[2][col] = vec.z;
	mat[3][col] = vec.w;
}



/* Matlab-friendly print function */
template<class T>
inline void mat4<T>::print()
{
	char buf[4096];

	sprintf(buf, "[ %f %f %f %f ;\n %f %f %f %f ;\n %f %f %f %f ;\n %f %f %f %f ]\n",
		a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p);

	printf("%s\n", buf);
}


















#endif /* RC_VMATH */



