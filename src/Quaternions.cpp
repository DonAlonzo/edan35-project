
#if 0

#include "RenderChimp.h"
#include "VectorMath.h"
#include "Quaternions.h"

quat::quat()
{
}

quat::quat(
		const quat		&nvec
	)
{
	x = nvec.x;
	y = nvec.y;
	z = nvec.z;
	w = nvec.w;
}

quat::quat(
		f32				nx,
		f32				ny,
		f32				nz,
		f32				nw
	)
{
	x = nx;
	y = ny;
	z = nz;
	w = nw;
}

quat::quat(
		vec3f			nvec,
		f32				nw
	)
{
	x = nvec.x;
	y = nvec.y;
	z = nvec.z;
	w = nw;
}

quat::quat(
		vec4f			nvec
	)
{
	x = nvec.x;
	y = nvec.y;
	z = nvec.z;
	w = nvec.w;
}


/* Assignment */
void quat::operator=(
		const quat		&v
	)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;
}

void quat::operator=(
		const vec4f		&v
	)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;
}

void quat::operator=(
		const mat4f		&v
	)
{
	fromMatrix(v);
}

void quat::operator=(
		const mat3f		&v
	)
{
	fromMatrix(v);
}


/* Comparison */
bool quat::operator==(
		const quat		&v
	)
{
	return x == v.x &&
		y == v.y &&
		z == v.z &&
		w == v.w;
}

bool quat::operator!=(
		const quat		&v
	)
{
	return x != v.x ||
		y != v.y ||
		z != v.z ||
		w != v.w;
}


/* Addition */
quat quat::operator+(
		const quat		&v
	)
{
	quat q = quat(1, 1, 1, 1);

	return q;
}

void quat::operator+=(
		const quat		&v
	)
{
	x += v.x;
	y += v.y;
	z += v.z;
	w += v.w;
}

quat quat::operator+()
{
	return quat(x, y, z, w);
}


/* Subtraction */
quat quat::operator-(
		const quat		&v
	)
{
	return quat(x - v.x,
		y - v.y,
		z - v.z,
		w - v.w);
}

void quat::operator-=(
		const quat		&v
	)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	w -= v.w;
}

quat quat::operator-()
{
	return quat(-x, -y, -z, -w);
}


/* Multiplication */
quat quat::operator*(
		const quat		&v
	)
{
	vec3f v3 = vec3f(x, y, z);
	vec3f vv3 = vec3f(v.x, v.y, v.z);
	vec3f nv3 = v3.cross(vv3) + vv3 * w + v3 * v.w;
	f32 nw = w * v.w - v3.dot(vv3);

	return quat(nv3, nw);
}

void quat::operator*=(
		const quat		&v
	)
{
	vec3f v3 = vec3f(x, y, z);
	vec3f vv3 = vec3f(v.x, v.y, v.z);
	vec3f nv3 = v3.cross(vv3) + vv3 * w + v3 * v.w;
	f32 nw = w * v.w - v3.dot(vv3);

	x = nv3.x;
	y = nv3.y;
	z = nv3.z;
	w = nw;
}


/* ... */
void quat::conjugate()
{
	x = -x;
	y = -y;
	z = -z;
}

f32 quat::norm()
{
	return x * x + y * y + z * z + w * w;
}

void quat::normalize()
{
	f32 inv_norm = 1.0f / norm();

	x *= inv_norm;
	y *= inv_norm;
	z *= inv_norm;
	w *= inv_norm;
}

void quat::inverse()
{
	quat p = quat(x, y, z, w);
	f32 inv_norm = 1.0f / norm();

	p.conjugate();

	x = p.x * inv_norm;
	y = p.y * inv_norm;
	z = p.z * inv_norm;
	w = p.w * inv_norm;
}

void quat::fromMatrix(
		mat3f			v
	)
{
	f32 tr, s;
	
	tr = v.t[0][0] + v.t[1][1] + v.t[2][2];

	if (tr >0.0f) {

		s = sqrt(tr + 1.0f);
		w = s * 0.5f;

		s = 0.5f / s;
		x = (v.t[2][1] - v.t[1][2]) * s;
		y = (v.t[0][2] - v.t[2][0]) * s;
		z = (v.t[1][0] - v.t[0][1]) * s;

	} else {

		vec3f v3;

		i32 i = 0, j, k;
		const i32 nxt[3] = {1, 2, 0};

		if (v.t[1][1] > v.t[0][0])
			i = 1;

		if (v.t[2][2] > v.t[i][i])
			i = 2;

		j = nxt[i];
		k = nxt[j];

		s = sqrt((v.t[i][i] - (v.t[j][j] + v.t[k][k])) + 1.0f);

		//v3.s[i] = s * 0.5f;
		v3.s[i] = s * 0.5f;

		if (s != 0.0f)
			s = 0.5f / s;

		w = (v.t[k][j] - v.t[j][k]) * s;
		v3.s[j] = (v.t[j][i] + v.t[i][j]) * s;
		v3.s[k] = (v.t[k][i] + v.t[i][k]) * s;

		x = v3.x;
		y = v3.y;
		z = v3.z;
	}
	/*
	f32 tr, s;
	
	tr = v.trace();

	if (tr >0.0f) {

		s = sqrt(tr + 1.0f);
		w = s * 0.5f;

		s = 0.5f / s;
		x = (v.t[1][2] - v.t[2][1]) * s;
		y = (v.t[2][0] - v.t[0][2]) * s;
		z = (v.t[0][1] - v.t[1][0]) * s;

	} else {

		i32 i = 0, j, k;
		const i32 nxt[3] = {1, 2, 0};

		if (v.t[1][1] > v.t[0][0])
			i = 1;

		if (v.t[2][2] > v.t[i][i])
			i = 2;

		j = nxt[i];
		k = nxt[j];

		s = sqrt((v.t[i][i] - (v.t[j][j] + v.t[k][k])) + 1.0f);

		v3.s[i] = s * 0.5f;

		if (s != 0.0f)
			s = 0.5f / s;

		w = (v.t[j][k] - v.t[k][j]) * s;
		v3.s[j] = (v.t[i][j] + v.t[j][i]) * s;
		v3.s[k] = (v.t[i][k] + v.t[k][i]) * s;
	}
	*/
}

void quat::fromMatrix(
		mat4f			v
	)
{
	mat3f m = v.rotationMatrix();

	fromMatrix(m);
}

mat3f quat::unitTo3x3Matrix()
{
	mat3f m;

	f32 x2 = x + x;
	f32 y2 = y + y;
	f32 z2 = z + z;

	f32 xx = x * x2;
	f32 xy = x * y2;
	f32 xz = x * z2;

	f32 yy = y * y2;
	f32 yz = y * z2;
	f32 zz = z * z2;

	f32 wx = w * x2;
	f32 wy = w * y2;
	f32 wz = w * z2;

	m.a = 1.0f - (yy + zz);
	m.b = xy - wz;
	m.c = xz + wy;

	m.d = xy + wz;
	m.e = 1.0f - (xx + zz);
	m.f = yz - wx;

	m.g = xz - wy;
	m.h = yz + wx;
	m.i = 1.0f - (xx + yy);

	return m;
}

mat4f quat::unitTo4x4Matrix()
{
	mat3f m3 = unitTo3x3Matrix();

	return mat4f(m3.a, m3.b, m3.c, 0.0f,
		m3.d, m3.e, m3.f, 0.0f,
		m3.g, m3.h, m3.i, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
}

void quat::slerp(
		quat		from,
		quat		to,
		f32			t
	)
{
	f32 to1[4];
	f32 cosom;
	f32 s0, s1;

	cosom = from.x * to.x + from.y * to.y +
		from.z * to.z + from.w * to.w;

	if (cosom < 0.0f) {
		cosom = - cosom;
		to1[0] = - to.x;
		to1[1] = - to.y;
		to1[2] = - to.z;
		to1[3] = - to.w;
	} else {
		to1[0] = to.x;
		to1[1] = to.y;
		to1[2] = to.z;
		to1[3] = to.w;
	}

	if ((1.0f - cosom) > 0.15f) {

		f32 omega = acos(cosom);
		f32 sinom_inv = 1.0f / sin(omega);
		s0 = sin((1.0f - t) * omega) * sinom_inv;
		s1 = sin(t * omega) * sinom_inv;

	} else {

		s0 = 1.0f - t;
		s1 = t;

	}

	x = s0 * from.x + s1 * to1[0];
	y = s0 * from.y + s1 * to1[1];
	z = s0 * from.z + s1 * to1[2];
	w = s0 * from.w + s1 * to1[3];

}

#endif

