

#include "RenderChimp.h"


/*
	Circle ring

	@param res_radius
	tesselation resolution (nbr of vertices) in the radial direction ( inner_radius < radius < outer_radius )

	@param res_theta
	tesselation resolution (nbr of vertices) in the angular direction ( 0 < theta < 2PI )

	@param inner_radius
	radius of the innermost border of the ring

	@param outer_radius
	radius of the outermost border of the ring

	@return
	pointer to the generated VertexArray object

	note: a parameter 'p' passed as 'const &p' 

	CJG aug 2010 - created
*/
VertexArray* createCircleRing(const i32 &res_radius, const i32 &res_theta, const f32 &inner_radius, const f32 &outer_radius)
{
	/* vertex definition */
	struct Vertex
	{
		f32 x, y, z,			/* spatial coordinates */
			texx, texy, texz,	/* texture coordinates */
			nx, ny, nz,			/* normal vector */
			tx, ty, tz,			/* tangent vector */
			bx, by, bz;			/* binormal vector */
	};

	/* vertex array */
	Vertex	*va = new Vertex[res_radius*res_theta];

	f32 theta = 0.0f,						/* 'stepping'-variable for theta: will go 0 - 2PI */
		dtheta = 2.0f*fPI/(res_theta-1);	/* step size, depending on the resolution */

	f32	radius,													/* 'stepping'-variable for radius: will go inner_radius - outer_radius */
		dradius = (outer_radius-inner_radius)/(res_radius-1);	/* step size, depending on the resolution */

	/* generate vertices iteratively */
	for (int i = 0; i < res_theta; i++)
	{
		f32	cos_theta = cos(theta),
			sin_theta = sin(theta);
		radius = inner_radius;

		for (int j = 0; j < res_radius; j++)
		{
			i32 vertex_index = res_radius*i + j;

			/* vertex */
			va[vertex_index].x = radius * cos_theta;
			va[vertex_index].y = radius * sin_theta;
			va[vertex_index].z = 0.0f;
			
			/* texture coordinates */
			va[vertex_index].texx = (f32)j/(res_radius-1);
			va[vertex_index].texy = (f32)i/(res_theta-1);
			va[vertex_index].texz = 0.0f;

			/* tangent */
			vec3f t = vec3f(cos_theta,
							sin_theta,
							0);
			t.normalize();

			/* binormal */
			vec3f b = vec3f(-sin_theta,
							cos_theta,
							0);
			b.normalize();
			
			/* normal */
			vec3f n = t.cross( b );
			
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;

			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;

			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;
			
			radius += dradius;
		}

		theta += dtheta;
	}

	/* triangle def */
	struct Triangle
	{
		i32 a, b, c;		/* vertex indices */
	};

	/* create index array */
	Triangle *ia = new Triangle[ 2*(res_theta-1)*(res_radius-1) ];

	/* generate indices iteratively */
	for (int i = 0; i < res_theta-1; i++ )
	{
		for (int j = 0; j < res_radius-1; j++ )
		{
			int tri_index = 2 * ((res_radius-1)*i + j);

			ia[tri_index].a = res_radius*i+j;
			ia[tri_index].b = res_radius*i+j + 1;
			ia[tri_index].c = res_radius*i+j + 1+res_radius;

			ia[tri_index+1].a = res_radius*i+j;
			ia[tri_index+1].b = res_radius*i+j + 1+res_radius;
			ia[tri_index+1].c = res_radius*i+j + res_radius;	
		}
	}

	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	VertexArray *cring_va = SceneGraph::createVertexArray(0, va, 15*sizeof(f32), res_radius*res_theta, TRIANGLES, USAGE_STATIC);
	cring_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);

	/* set the index array */
	cring_va->setIndexArray(ia, sizeof(i32), 2*3*(res_radius-1)*(res_theta-1) );

	/* va and ia are copied within createVertexArray so we can safely release their memory */
	delete [] va;
	delete [] ia;

	return cring_va;
}


/*
	Sphere

	0 < theta < PI
	0 < phi < 2PI

	CJG aug 2010 - created
*/
VertexArray* createSphere(const i32 &res_theta, const i32 &res_phi, const f32 &radius)
{
	/* vertex definition */
	struct Vertex
	{
		f32 x, y, z,			/* spatial coordinates */
			texx, texy, texz,	/* texture coordinates */
			nx, ny, nz,			/* normal vector */
			tx, ty, tz,			/* tangent vector */
			bx, by, bz;			/* binormal vector */
	};

	/* vertex array */
	Vertex	*va = new Vertex[res_theta*res_phi];

	f32 theta = 0.0f,						/* 'stepping'-variable for theta: will go 0 - 2PI */
		dtheta = 2.0f*fPI/(res_theta-1);	/* step size, depending on the resolution */

	f32	phi = 0.0f,							/* 'stepping'-variable for phi: will go 0 - 2PI */
		dphi = fPI/(res_phi-1);				/* step size, depending on the resolution */

	/* generate vertices iteratively */
	for (int i = 0; i < res_phi; i++)
	{
		f32	cos_phi = cos(phi),
			sin_phi = sin(phi);
		theta = 0.0f;

		for (int j = 0; j < res_theta; j++)
		{
			f32	cos_theta = cos(theta),
				sin_theta = sin(theta);
			int vertex_index = res_theta*i + j;

			/* vertex */
			va[vertex_index].x = radius * sin_theta * sin_phi;
			va[vertex_index].y = -radius * cos_phi;
			va[vertex_index].z = radius * cos_theta * sin_phi;

			/* texture coordinates */
			va[vertex_index].texx = (f32)j/(res_theta-1)*1;
			va[vertex_index].texy = (f32)i/(res_phi-1)*1;
			va[vertex_index].texz = 0.0f;
			
			/* tangent */
			vec3f t = vec3f(cos_theta,
							0,
							-sin_theta);
			t.normalize();

			/* binormal */
			vec3f b = vec3f(sin_theta * cos_phi,
							sin_phi,
							cos_theta * cos_phi);
			b.normalize();
			
			/* normal */
			vec3f n = t.cross( b );
			
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;

			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;

			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;

			theta += dtheta;
		}

		phi += dphi;
	}

	/* triangle def */
	struct Triangle
	{
		i32 a, b, c;		/* vertex indices */
	};

	/* create index array */
	Triangle *ia = new Triangle[ 2*(res_theta-1)*(res_phi-1) ];

	/* generate indices iteratively */
	for (int i = 0; i < res_phi-1; i++ )
	{
		for (int j = 0; j < res_theta-1; j++ )
		{
			int tri_index = 2 * ((res_theta-1)*i + j);

			ia[tri_index].a = res_theta*i+j;
			ia[tri_index].b = res_theta*i+j + 1;
			ia[tri_index].c = res_theta*i+j + 1+res_theta;

			ia[tri_index+1].a = res_theta*i+j;
			ia[tri_index+1].b = res_theta*i+j + 1+res_theta;
			ia[tri_index+1].c = res_theta*i+j + res_theta;	
		}
	}

	/* initialize the scene graph vertex array and set attributes */
	VertexArray *vertex_array = SceneGraph::createVertexArray("rtest", va, 15*sizeof(f32), res_theta*res_phi, TRIANGLES, USAGE_STATIC);
	vertex_array->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);

	/* set the index array */
	vertex_array->setIndexArray(ia, sizeof(i32), 2*3*(res_theta-1)*(res_phi-1) );

	/* va and ia are copied within createVertexArray so we can safely release their memory */
	delete [] va;
	delete [] ia;

	return vertex_array;
}



/*
	Torus

	rA: radius from shape center to tube center
	rB: radius of tube
	0 < theta, phi < 2PI

	CJG aug 2010 - created
*/
VertexArray* createTorus(const i32 &res_theta, const i32 &res_phi, const f32 &rA, const f32 &rB)
{
	/* vertex definition */
	struct Vertex
	{
		f32 x, y, z,			/* spatial coordinates */
			texx, texy, texz,	/* texture coordinates */
			nx, ny, nz,			/* normal vector */
			tx, ty, tz,		/* tangent vector */
			bx, by, bz;		/* binormal vector */
	};

	/* vertex array */
	Vertex	*va = new Vertex[res_theta*res_phi];

	f32 theta = 0.0f,						/* 'stepping'-variable for theta: will go 0 - 2PI */
		dtheta = 2.0f*fPI/(res_theta-1);	/* step size, depending on the resolution */

	f32	phi = 0.0f,							/* 'stepping'-variable for phi: will go 0 - 2PI */
		dphi = 2.0f*fPI/(res_phi-1);		/* step size, depending on the resolution */

	/* generate vertices iteratively */
	for (int i = 0; i < res_phi; i++)
	{
		f32	cos_phi = cos(phi),
			sin_phi = sin(phi);
		theta = 0.0f;

		for (int j = 0; j < res_theta; j++)
		{
			f32	cos_theta = cos(theta),
				sin_theta = sin(theta);
			int vertex_index = res_theta*i + j;

			/* vertex */
			va[vertex_index].x = (rA + rB * cos_theta) * cos_phi;
			va[vertex_index].y = (rA + rB * cos_theta) * sin_phi;
			va[vertex_index].z = -rB * sin_theta;
			
			/* texture coordinates */
			va[vertex_index].texy = (f32)i/(res_phi-1)*2;
			va[vertex_index].texx = (f32)j/(res_theta-1)*2;
			va[vertex_index].texz = 0.0f;

			/* tangent */
			vec3f t(-sin_theta * cos_phi,
					-sin_theta * sin_phi,
					-cos_theta );
			t.normalize();

			/* binormal */
			vec3f b(-(rA + rB * cos_theta) * sin_phi,
					(rA + rB * cos_theta) * cos_phi,
					0 );
			b.normalize();

			/* normal */
			vec3f n = t.cross( b );

			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;			
			
			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;

			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;

			theta += dtheta;
		}

		phi += dphi;
	}

	/* triangle definition */
	struct Triangle
	{
		i32 a, b, c;		/* vertex indices */
	};

	/* create index array */
	Triangle *ia = new Triangle[ 2*(res_theta-1)*(res_phi-1) ];

	/* generate indices iteratively */
	for (int i = 0; i < res_phi-1; i++ )
	{
		for (int j = 0; j < res_theta-1; j++ )
		{
			int tri_index = 2 * ((res_theta-1)*i + j);

			ia[tri_index].a = res_theta*i+j;
			ia[tri_index].b = res_theta*i+j + 1;
			ia[tri_index].c = res_theta*i+j + 1+res_theta;

			ia[tri_index+1].a = res_theta*i+j;
			ia[tri_index+1].b = res_theta*i+j + 1+res_theta;
			ia[tri_index+1].c = res_theta*i+j + res_theta;	
		}
	}

	/* initialize the scene graph vertex array and set attributes */
	VertexArray *vertex_array = SceneGraph::createVertexArray(0, va, 15*sizeof(f32), res_theta*res_phi, TRIANGLES, USAGE_STATIC);
	vertex_array->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	vertex_array->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);

	/* set the index array */
	vertex_array->setIndexArray(ia, sizeof(i32), 2*3*(res_theta-1)*(res_phi-1) );

	/* va and ia are copied within createVertexArray so we can safely release their memory */
	delete [] va;
	delete [] ia;

	return vertex_array;
}

