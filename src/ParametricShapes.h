
#ifndef RC_PARAMETRIC_SHAPES_H
#define RC_PARAMETRIC_SHAPES_H

/*
	Circle ring

	inner_radius < radius < outer_radius
	0 < theta < 2PI

	CJG aug 2010 - created
*/
VertexArray* createCircleRing(const i32 &radius_res, const i32 &theta_res, const f32 &inner_radius, const f32 &outer_radius);

/*
	Sphere

	0 < theta < PI
	0 < phi < 2PI

	CJG sep 2010 - created
*/
VertexArray* createSphere(const i32 &res_theta, const i32 &res_phi, const f32 &radius);

/*
	Torus

	rA: radius from shape center to tube center
	rB: radius of tube
	0 < theta, phi < 2PI

	CJG sep 2010 - created
*/
VertexArray* createTorus(const i32 &res_theta, const i32 &res_phi, const f32 &rA, const f32 &rB);

#endif /* RC_PARAMETRIC_SHAPES_H */

