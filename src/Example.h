#ifndef RC_EXAMPLE_H
#define RC_EXAMPLE_H

//#define RC_COMPILE_EXAMPLE
//#define RC_ACTIVE_EXAMPLE			1

//#define RC_COMPILE_BENCHMARK
//#define RC_ACTIVE_BENCHMARK			1


#if defined RC_COMPILE_EXAMPLE && defined RC_COMPILE_BENCHMARK
#pragma warning("Conflicting example and benchmark. Define either RC_COMPILE_BENCHMARK or RC_COMPILE_EXAMPLE, or none of them. Compiling benchmark only...")
#undef RC_COMPILE_EXAMPLE
#endif

#endif /* RC_EXAMPLE_H */

