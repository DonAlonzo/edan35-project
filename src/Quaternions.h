/*
 * TODO: Euler to quaternions
 * TODO: Non unit to matrix conversions
 * TODO: Lots and lots of testing
 *
 * Much of this code is shamelessly ripped from
 * http://www.gamasutra.com/view/feature/3278/rotating_objects_using_quaternions.php
 */

#ifndef RC_QUATERNIONS_H
#define RC_QUATERNIONS_H

class quat
{
	public:

		/* Members */

		quat();

		quat(
				const quat		&nvec
			);

		quat(
				f32				nx,
				f32				ny,
				f32				nz,
				f32				nw
			);

		quat(
				vec3f			nvec,
				f32				nw
			);

		quat(
				vec4f			nvec
			);


		/* Assignment */
		void operator=(
				const quat		&v
			);

		void operator=(
				const vec4f		&v
			);

		void operator=(
				const mat4f		&v
			);

		void operator=(
				const mat3f		&v
			);


		/* Comparison */
		bool operator==(
				const quat		&v
			);

		bool operator!=(
				const quat		&v
			);


		/* Addition */
		quat operator+(
				const quat		&v
			);

		void operator+=(
				const quat		&v
			);

		quat operator+();


		/* Subtraction */
		quat operator-(
				const quat		&v
			);

		void operator-=(
				const quat		&v
			);

		quat operator-();


		/* Multiplication */
		quat operator*(
				const quat		&v
			);

		void operator*=(
				const quat		&v
			);


		/* ... */
		void conjugate();

		f32 norm();

		void normalize();

		void inverse();

		void fromMatrix(
				mat3f			v
			);

		void fromMatrix(
				mat4f			v
			);

		mat3f unitTo3x3Matrix();

		mat4f unitTo4x4Matrix();

	void slerp(
			quat		from,
			quat		to,
			f32			t
		);

	public:

		f32			x;
		f32			y;
		f32			z;
		f32			w;

};


//typedef Quaternions			quat;

#endif /* RC_QUATERNIONS_H */

