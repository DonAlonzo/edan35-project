#include "Example.h"

#if defined RC_COMPILE_EXAMPLE && RC_ACTIVE_EXAMPLE == 0

/*
 * Example 00 - Hello triangle!
 *
 * Lessons learned:
 *   - Render a single triangle using a default shader
 *
 */

#include "RenderChimp.h"

/* A world node is needed as the root of the scene graph */
World *world;

/* A camera is used to specify how, and from where, the scene is viewed */
Camera *camera;

/* The triangle node */
Geometry *triangle;

/* A series of vertex positions to be used in the vertex array */
/* Vertex.xyz */
f32 attribArray[3 * 3] = {
	-3.0f, -3.0f, 0.0f,
	3.0f, -3.0f, 0.0f,
	0.0f, 3.0f, 0.0f
};

/* The vertex array defines the mesh, which the geometry node "triangle
 * will use.
 */
VertexArray *vertexArray;

/* Shader programs are renderer specific pipeline steps */
ShaderProgram *shader;

/* Start time of triangle animation */
f32 starttime;

/* RCInit is called once, at the startup of the program */
void RCInit()
{
	/* First we need a root node for our scene */
	world = SceneGraph::createWorld("MyWorld");

	/* Next we create the camera from which to view the scene */
	camera = SceneGraph::createCamera("MyCamera");
	
	/* The parameters to the camera are explained in Camera.h */
	camera->setPerspectiveProjection(1.5f, 4.0f / 3.0f, 1.0f, 1000.0f);

	/* Move the camera up and back a bit (yes, 10.0f actually moves the camera
	 * back since it's viewing the scene in the negative Z-direction. Also, tilt
	 * the camera down a little around its X-axis to better view the scene.
	 */
	camera->translate(0.0f, 2.0f, 10.0f);
	camera->rotateX(-0.3f);

	/* The camera is attached as a child to the root node, and it is specified
	 * to be the view point. There may exist any number of cameras in your
	 * scene, and you can switch between them using the setActiveCamera() call.
	 * The camera used may even belong to another scene graph altogether.
	 */
	world->attachChild(camera);
	world->setActiveCamera(camera);

	/* Set up the mesh consisting of one single triangle. The arguments to this
	 * function is explained in detail in SceneGraph.h
	 *   The attribArray argument contains the actual position data of the
	 * triangle.
	 */
	vertexArray = SceneGraph::createVertexArray("MyVertexArray", attribArray, 3 * sizeof(f32), 3, TRIANGLES, USAGE_STATIC);

	/* Tell the shader program (further down the rendering pipeline, don't
	 * worry about it just yet...) how to utilize the data in the attribArray
	 * argument sent to SceneGraph::createVertexArray().
	 */
	vertexArray->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32, false);

	/* Attach the mesh data to the geometry node so that it can be drawn */
	triangle = SceneGraph::createGeometry("MyTriangle", vertexArray, true);

	/* Now just attach the triangle geometry node to the world node, and the
	 * scene is set to be rendered!
	 */
	world->attachChild(triangle);

	/* This is described in more detail in Example01 */
	shader = SceneGraph::createShaderProgram("MyShader", "Default", 0);
	triangle->setShaderProgram(shader);
	
	/* Start timer */
	starttime = Platform::getFrameTime();

	/* Log a message to the console */
	Console::log("Example 00 - Hello triangle!");

	/* Set clear color and depth */
	Renderer::setClearColor(vec4f(0.1f, 0.2f, 0.3f, 1.0f));
	Renderer::setClearDepth(1.0f);
}

/* This is called once each frame (as often as possible) */
u32 RCUpdate()
{
	/* Get the time when this frame started being processed */
	f32 time = Platform::getFrameTime();

	/* Rotate the triangle around a little to show that it's actually 3D */
	triangle->setRotateY(sin(time * 0.8f) * 1.2f);
	triangle->rotateX(sin(time * 0.15f) * 0.5f);

	/* Clear color and depth buffers */
	Renderer::clearColor();
	Renderer::clearDepth();
	
	/* After all transformations are done it's time to render the entire scene
	 * graph. It's all taken care of using the renderAll() call
	 */
	world->renderAll();

	/* Return 0 for as long as the program should run (otherwise !0) */
	return 0;
}

/* This is called once - when the program is terminated */
void RCDestroy()
{
	/* Delete the world node. All attach children are also recursively
	 * deleted.
	 */
	SceneGraph::deleteNode(world);
}



#endif /* RC_COMPILE_EXAMPLE */


