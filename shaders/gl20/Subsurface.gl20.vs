attribute vec3 Vertex;
attribute vec3 Normal;
attribute vec2 Texcoord;

uniform mat4 World;
uniform mat4 WorldViewProjection;
uniform mat4 WorldInverseTranspose;
uniform vec3 LightPosition;

varying vec3 worldNormal;
varying vec3 eyeVec;
varying vec3 lightVec;
varying vec3 vertPos;
varying vec3 lightPos;
varying vec2 texcoord;
 
void subScatterVS(in vec4 ecVert) {
    lightVec = LightPosition - ecVert.xyz;
    eyeVec = -ecVert.xyz;
    vertPos = ecVert.xyz;
    lightPos = LightPosition;
}

void main() {
	worldNormal = (WorldInverseTranspose * vec4(Normal.xyz, 1.0)).xyz;
    vec4 ecPos = WorldViewProjection * vec4(Vertex.xyz, 1.0);

    subScatterVS(ecPos);

    gl_Position = ecPos;
    texcoord = Texcoord;
}