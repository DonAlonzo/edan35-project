
attribute vec3 Vertex;

uniform mat4 WorldViewProjection;

void main() {

	// Position
	gl_Position = WorldViewProjection * vec4(Vertex.xyz, 1.0);
}



