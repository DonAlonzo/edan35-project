uniform vec3 lcolor;

varying vec2 texc;
varying vec3 nrm;

void main()
{
	gl_FragColor = vec4(lcolor, 1.0);
}

