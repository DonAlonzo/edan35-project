

uniform vec3	LightPosition,
				CameraPosition;
				
uniform vec3	AmbientColor,
				DiffuseColor,
				SpecColor;
uniform float	Shininess;

varying vec3	varying_position,
				varying_normal,
				varying_world_pos;
varying vec2	varying_tex;

varying vec3	varying_tangent,
				varying_binormal;

uniform sampler2D	DiffuseTex,
					BumpTex;
uniform samplerCube	CubeTex;

uniform int doTex, doCube, doBump;

void main()
{
	vec3 varying_normal_norm	= normalize( varying_normal );
	vec3 varying_tangent_norm	= normalize( varying_tangent );
	vec3 varying_binormal_norm	= normalize( varying_binormal );
	
	vec3 light_vec_norm			= normalize( LightPosition - varying_position );
	vec3 view_vec_norm			= normalize( CameraPosition - varying_position );
	
	// normal from bumpmap 
	vec3 bump_normal = texture2D(BumpTex, varying_tex) * 2.0 - 1.0;
	mat3 TBN = mat3( varying_tangent_norm, varying_binormal_norm, varying_normal_norm );
	if (doBump)
		varying_normal_norm = normalize( TBN * bump_normal );

	// ambient
	vec3 color = AmbientColor;
	
	// A. diffuse interpolated
	color += DiffuseColor * max( 0.0, dot( varying_normal_norm, light_vec_norm ) );
	
	// B. diffuse from cube map
	vec3 view_refl_norm = normalize( reflect( -view_vec_norm, varying_normal_norm ) );
	if (doCube)
		color = textureCube(CubeTex, view_refl_norm);
	
	// C. diffuse from texture
	if (doTex)
		color *= texture2D(DiffuseTex, varying_tex);

	// spec
	vec3 light_refl_norm	= normalize( reflect( -light_vec_norm, varying_normal_norm ) );
	float spec				= pow( max(0.0, dot( light_refl_norm, view_vec_norm ) ), Shininess );
	color					+= SpecColor * spec;
	
	//color = varying_binormal_norm / 2.0 + 0.5;
	gl_FragColor = vec4(color.xyz, 1.0);
}