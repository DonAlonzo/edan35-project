
attribute vec3 Vertex;
attribute vec3 Normal;

uniform mat4 WorldViewProjection;
uniform mat4 World ;

varying vec3 varying_normal;

void main() {

	vec4 world_normal = World * vec4(Normal, 0.0);
	varying_normal = world_normal.xyz;

    gl_Position = WorldViewProjection * vec4(Vertex, 1.0);
}



