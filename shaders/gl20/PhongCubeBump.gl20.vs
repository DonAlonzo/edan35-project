

attribute vec3	Vertex,
				Normal,
				Tangent,
				Binormal,
				Texcoord;

uniform mat4	WorldViewProjection;
uniform mat4	WorldInverseTranspose;
uniform mat4	World;

varying vec3	varying_position,
				varying_normal;
varying vec2	varying_tex;

varying vec3	varying_tangent,
				varying_binormal;
				
void main()
{
	varying_position = ( World * vec4(Vertex.xyz, 1.0) ).xyz;
	
	varying_normal = ( WorldInverseTranspose * vec4(Normal.xyz, 1.0) ).xyz;
	
	varying_tangent = ( WorldInverseTranspose * vec4(Tangent.xyz, 1.0) ).xyz;
	varying_binormal = ( WorldInverseTranspose * vec4(Binormal.xyz, 1.0) ).xyz;
	
	varying_tex = vec2(Texcoord.x, Texcoord.y);
	
	gl_Position = WorldViewProjection * vec4( Vertex.xyz, 1.0 );
}