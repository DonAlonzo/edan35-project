uniform vec3 LightPosition;
uniform vec3 LightColor;

uniform	vec3 AmbientColor;
uniform	vec3 DiffuseColor;
uniform	float Emissive;
uniform	vec3 SpecularColor;
uniform	float Shininess;
uniform	float SpecularLevel;

uniform sampler2D DiffuseTex;

varying	vec2 texcoord;
varying	vec3 worldPosition;
varying	vec3 worldNormal;
varying	vec3 viewVector;
varying vec3 lightVector;

void main() {

	vec3 normWorldNormal = normalize(worldNormal);
	vec3 normLightVector = normalize(lightVector);

	float lightNormalCos = max(Emissive, dot(normWorldNormal, normLightVector));

	  // Ambient, Emissive and Diffuse

	vec4 color = vec4(AmbientColor + LightColor * lightNormalCos * DiffuseColor, 1.0);

	    // Texture
	color *= texture2D(DiffuseTex, texcoord);

	if (SpecularLevel > 0.0) {

		vec3 reflected = -reflect(normLightVector, normWorldNormal);
		float s = max(0.0, dot(normalize(reflected), normalize(viewVector)));
		vec3 specular = LightColor * pow(s, Shininess) * SpecularLevel * SpecularColor;

			// Specular
		color += vec4(specular, 0.0);
	}

		// Write final color to color buffer
	gl_FragColor = color;

}

