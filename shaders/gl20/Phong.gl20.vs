attribute vec3 Vertex;
attribute vec3 Normal;
attribute vec3 Texcoord;

uniform mat4 World;
uniform mat4 WorldViewProjection;
uniform mat4 WorldInverseTranspose;
uniform vec3 ViewPosition;

uniform vec3 LightPosition;
uniform vec3 LightColor;

uniform vec3 AmbientColor;
uniform vec3 DiffuseColor;
uniform float Emissive;
uniform vec3 SpecularColor;
uniform float Shininess;
uniform float SpecularLevel;


	// To fragment shader

varying vec2 texcoord;
varying vec3 worldPosition;
varying vec3 worldNormal;
varying vec3 viewVector;
varying vec3 lightVector;



void main() {

		// Set up interpolated values
	worldPosition = (World * vec4(Vertex.xyz, 1.0)).xyz;
	worldNormal = (WorldInverseTranspose * vec4(Normal.xyz, 1.0)).xyz;

	viewVector = normalize(ViewPosition - worldPosition);
	lightVector = (LightPosition - worldPosition);

	texcoord = vec2(Texcoord.x, Texcoord.y);

		// Position
	gl_Position = WorldViewProjection * vec4(Vertex.xyz, 1.0);

}

