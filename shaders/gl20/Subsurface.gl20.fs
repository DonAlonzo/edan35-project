uniform sampler2D DiffuseTex;
uniform vec4 LightColor;
uniform vec4 SpecularColor;
uniform vec4 ScatterColor;
uniform float ScatterWidth;
uniform float Wrap;
uniform float Shininess;

varying vec3 worldNormal;
varying vec3 eyeVec;
varying vec3 lightVec;
varying vec3 vertPos;
varying vec3 lightPos;
varying vec2 texcoord;

void main() {
    vec3 halfVec = lightVec + eyeVec;
    halfVec = halfVec / length(halfVec);
    
    float NdotL = dot(worldNormal, lightVec) * 2 - 1;
    float NdotH = dot(worldNormal, halfVec) * 2 - 1;
    float NdotL_wrap = (NdotL + Wrap) / (1 + Wrap);
    float diffuse = max(NdotL_wrap, 0.0f);
    float scatter = smoothstep(0.0f, ScatterWidth, NdotL_wrap) *
                    smoothstep(ScatterWidth * 2.0f, ScatterWidth, NdotL_wrap);
    
    float specular = pow(NdotH, Shininess);
    if (NdotL_wrap <= 0.0f)
        specular = 0.0f;
    vec4 C = scatter * ScatterColor + diffuse;
    C.a = specular;
    
    vec3 diffColor = texture2D(DiffuseTex, texcoord.xy).rgb;
    gl_FragColor = vec4(diffColor * 0.5f + diffColor * C.rgb  + ScatterColor.rgb * C.a, 1.0f);
}