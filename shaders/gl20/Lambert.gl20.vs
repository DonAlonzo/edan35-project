/* Per vertex Lambert shader */

attribute vec3 Vertex;
attribute vec3 Normal;
attribute vec3 Texcoord;

uniform mat4 World;
uniform mat4 WorldViewProjection;
uniform mat4 WorldInverseTranspose;
uniform vec3 ViewPosition;

uniform vec3 LightPosition;
uniform vec3 LightColor;

	// To fragment shader

varying vec2 texcoord;
varying vec3 diffuse_color;


void main() {

	vec3 worldPosition = (World * vec4(Vertex.xyz, 1.0)).xyz;
	vec3 worldNormal = (WorldInverseTranspose * vec4(Normal.xyz, 1.0)).xyz;
	vec3 viewVector = normalize(ViewPosition - worldPosition);

		// Diffuse

	vec3 lightVector = normalize(LightPosition - worldPosition);

	float lightNormalCos = max(0.0, dot(normalize(worldNormal), lightVector));
	diffuse_color = LightColor * lightNormalCos;


	texcoord = vec2(Texcoord.x, Texcoord.y);

		// Position
	gl_Position = WorldViewProjection * vec4(Vertex.xyz, 1.0);

}



